import org.scalastyle.sbt.ScalastylePlugin
import com.trueaccord.scalapb.compiler.Version.scalapbVersion

enablePlugins(SbtScalariform)
enablePlugins(SbtLicenseReport)
enablePlugins(ScalastylePlugin)

lazy val akkaHttpVersion = "10.0.11"
lazy val akkaVersion    = "2.5.7"

// Seems to be an ugly work around for obtaining a task where compile can depend on ...
lazy val compileScalastyle = taskKey[Unit]("compileScalastyle")
compileScalastyle := scalastyle.in(Compile).toTask("").value

lazy val ivyLocal = Resolver.file("ivyLocal", file(Path.userHome.absolutePath + "/.ivy2/local"))(Resolver.ivyStylePatterns)

lazy val releaseSettings = Seq(
  publishMavenStyle := true,
  pomIncludeRepository := { _ => false },
  publishArtifact in Test := false,
    publishTo := {
    val repo = "https://oss.sonatype.org/"
    if (isSnapshot.value)
      Some("snapshots" at repo + "content/repositories/snapshots")
    else
      Some("releases" at repo + "service/local/staging/deploy/maven2")
  },
  pomExtra :=
    <url>https://bitbucket.org/processweavers/rbpl/wiki/Home</url>
    <scm>
      <connection>scm:git:git://bitbucket.org/bitbucket.org/processweavers/rbpl-core.git</connection>
      <developerConnection>scm:git:ssh://bitbucket.org:processweavers/rbpl-core.git</developerConnection>
      <url>https://bitbucket.org/processweavers/rbpl-core/src</url>
    </scm>
    <licenses>
      <license>
        <name>BSD-3-Clause</name>
        <url>https://opensource.org/licenses/BSD-3-Clause</url>
        <distribution>repo</distribution>
      </license>
    </licenses>
    <developers>
      <developer>
        <name>Carsten Seibert</name>
        <email>seibert@seibertec.ch</email>
        <organization>seiberTEC GmbH</organization>
        <organizationUrl>http://www.seibertec.ch</organizationUrl>
      </developer>
      <developer>
        <name>Anke Seibert-Otto</name>
        <email>anke.seibert@seibertec.ch</email>
        <organization>seiberTEC GmbH</organization>
        <organizationUrl>http://www.seibertec.ch</organizationUrl>
      </developer>
    </developers>
)

lazy val root = (project in file("."))
  .configs( IntegrationTest )
  .settings( Defaults.itSettings : _*)
  .settings(
      concurrentRestrictions in Global += Tags.limit(Tags.Test, 1),
      inThisBuild(List(
      organization    := "net.processweavers",
      version := "0.0.5-SNAPSHOT",
      scalaVersion    := "2.12.4",
      scalacOptions ++= Seq(
        "-unchecked",
        "-deprecation",
        "-Xexperimental",
        "-feature",
        "-language:implicitConversions",
        "-language:reflectiveCalls"
      )
    )),
    (compile in Compile) := ((compile in Compile) dependsOn compileScalastyle).value,
    name := "rbpl-core",
    resolvers += Resolver.jcenterRepo,

    // TODO: ONLY FOR LOCAL TESTING OF LIBRARY REFACTORING!!!
    // resolvers += ivyLocal,

    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http"        % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-actor"       % akkaVersion,
	    "com.typesafe.akka" %% "akka-persistence" % akkaVersion,
      "com.typesafe.akka" %% "akka-contrib"     % akkaVersion,
      "com.typesafe.akka" %% "akka-slf4j"       % akkaVersion,
      "com.typesafe.play" %% "play-json"        % "2.6.7",
      "org.typelevel"     %% "cats"             % "0.9.0",
      "com.chuusai"       %% "shapeless"        % "2.3.2",
      "de.heikoseeberger" % "akka-http-play-json_2.12" % "1.17.0",
      "ch.qos.logback"    % "logback-classic"   % "1.2.3",
      "com.typesafe.akka" %% "akka-testkit" % akkaVersion,
      "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion % "test",
      "net.processweavers" %% "akka-persistent-test-support"   % "0.0.1" % "test",
      "com.trueaccord.scalapb" %% "scalapb-runtime"  % scalapbVersion  % "protobuf"
    ),
    cleanFiles += { baseDirectory.value / "persistent" },
    PB.protoSources in Compile := Seq(sourceDirectory.value / "main" / "resources" / "rbplcore")
  )
  .settings(releaseSettings: _*)

PB.targets in Compile := Seq(
  scalapb.gen() -> (sourceManaged in Compile).value
)

PB.targets in Test := Seq(
  scalapb.gen() -> (sourceManaged in Test).value
)

inConfig(Test)(sbtprotoc.ProtocPlugin.protobufConfigSettings)