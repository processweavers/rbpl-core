package net.processweavers.rbpl.front

import akka.actor.{ Actor, ActorRef, Props }
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.headers.BasicHttpCredentials
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.testkit.{ TestActor, TestProbe }
import net.processweavers.rbpl.core.env.PrincipalProvider
import net.processweavers.rbpl.core.env.PrincipalProvider.{ GetPrincipal, Principal }
import net.processweavers.rbpl.core.process.Process.{ ActiveTasks, AllActiveTasks }
import net.processweavers.rbpl.core.process.{ CorrelationId, ProcessRepo }
import net.processweavers.rbpl.core.process.ProcessRepo.{ FindWithCorrelationId, FinderResult }
import net.processweavers.rbpl.core.task.TaskId
import net.processweavers.rbpl.util.{ MockPrincipalProvicerSupport, MockProcessRepoSupport, TestFixtures }
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{ Matchers, WordSpec }
import play.api.libs.json.Json

class ProcessRepoRouteTest extends WordSpec
  with Matchers
  with ScalatestRouteTest
  with ScalaFutures
  with TestFixtures
  with MockProcessRepoSupport
  with MockPrincipalProvicerSupport {

  val correlationId = CorrelationId("myCId")
  val processProbe = TestProbe()

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    setupMockProcessRepo
    setupMockPrincipalProvider
  }

  "A ProcessRepoRoute" must {
    "return all active tasks for a valid cId" in new TestScope {

      import net.processweavers.rbpl.core.task.Serializers._

      processRepoProbe.setAutoPilot((sender: ActorRef, msg: Any) => msg match {
        case FindWithCorrelationId(`correlationId`) =>
          sender ! FinderResult(Some((testProcessContext(correlationId), processProbe.ref))); TestActor.KeepRunning
        case m => system.log.warning(s"processRepoProbe: Unexpected message $m"); TestActor.NoAutoPilot
      })

      val taskId = TaskId.create
      processProbe.setAutoPilot((sender: ActorRef, msg: Any) => msg match {
        case AllActiveTasks =>
          sender ! ActiveTasks(Map(taskId -> ("testTask", TestProbe().ref))); TestActor.KeepRunning
        case m => system.log.warning(s"processProbe: Unexpected message $m"); TestActor.NoAutoPilot
      })

      Get(s"/processes/${correlationId.s}/activeTasks") ~> addPrincipal ~> routeToTest ~> check {
        status shouldBe StatusCodes.OK
        val res = Json.parse(entityAs[String]).as[List[(TaskId, String)]]
        res.head shouldBe (taskId, "testTask")
      }
    }

    "return all active tasks for a INvalid cId" in new TestScope {

      processRepoProbe.setAutoPilot((sender: ActorRef, msg: Any) => msg match {
        case FindWithCorrelationId(_) =>
          sender ! FinderResult(None); TestActor.KeepRunning
        case m => system.log.warning(s"processRepoProbe: Unexpected message $m"); TestActor.NoAutoPilot
      })

      Get(s"/processes/${correlationId.s}/activeTasks") ~> addPrincipal ~> routeToTest ~> check {
        handled shouldBe false
      }

      // Verify external view on sealed route
      Get(s"/processes/${correlationId.s}/activeTasks") ~> addPrincipal ~> Route.seal(routeToTest) ~> check {
        status shouldBe StatusCodes.NotFound
      }
    }
    //    "return all active tasks" in new TestScope {
    //      Get("/processes") ~> routeToTest ~> check {
    //
    //      }
    //    }
  }

  abstract class TestScope {
    def routeToTest = new ProcessRepoRoute {}.route
  }
}
