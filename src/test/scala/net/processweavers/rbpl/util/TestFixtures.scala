package net.processweavers.rbpl.util

import akka.actor.{ Actor, ActorRef, Props }
import akka.http.scaladsl.model.headers.BasicHttpCredentials
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.testkit.TestProbe
import net.processweavers.rbpl.core.Initializer
import net.processweavers.rbpl.core.env.PrincipalProvider
import net.processweavers.rbpl.core.env.PrincipalProvider.{ GetPrincipal, Principal }
import net.processweavers.rbpl.core.process._
import net.processweavers.rbpl.core.task._
import shapeless.HNil

trait MockPrincipalProvicerSupport { self: ScalatestRouteTest =>

  val principal = Principal("testId", "testPw")

  def setupMockPrincipalProvider =
    system.actorOf(Props(new Actor {
      def receive: Receive = {
        case GetPrincipal(_) =>
          sender ! principal
        case m => system.log.warning(s"PrincipalProviderMock: Unexpected message $m")
      }
    }), PrincipalProvider.ACTORNAME)

  def addPrincipal =
    addCredentials(BasicHttpCredentials(principal.id, principal.pw))
}

trait MockProcessRepoSupport { self: ScalatestRouteTest =>

  val processRepoProbe = TestProbe()

  def setupMockProcessRepo =
    system.actorOf(Props(new Actor {
      def receive: Receive = {
        case m => processRepoProbe.ref forward m
      }
    }), ProcessRepo.ACTORNAME)

}

trait TestFixtures {
  def testTaskDescriptor(taskName: String = "") = {
    TaskDescriptor(taskName, testProcessContext(), TaskContext(TaskId.create, None, None), "")
  }

  def testProcessContext(cId: CorrelationId = CorrelationId("")) = ProcessContext(ProcessId.create, cId, ProcessName(""))
}

object ProcessFixtures {

  case object EmptyPayload

  case class MyPayload(aString: String, poMsgProbe: ActorRef) {
    val name: String = classOf[MyProcess].getName
  }

  class Task1(val descriptor: TaskDescriptor, i: Initializer[Task1.Task1Payload]) extends Actor with Task {
    def receive: Receive = Actor.emptyBehavior
  }

  object Task1 {

    case class Task1Payload(a: String)

    implicit val activator1 = new TaskActivator[Task1Payload] {
      def propsFor(processContext: ProcessContext, taskContext: TaskContext, i: Initializer[Task1Payload])(): Props = Props(new Task1(TaskDescriptor(i.name, processContext, taskContext, i.description), i))
    }
  }

  class MyProcess(val ctx: ProcessContext, val initializer: Initializer[MyPayload]) extends BaseProcess(Initializer[Task1.Task1Payload]("", Task1.Task1Payload("")) :: HNil) {

    var state: String = ""

    def receiveRecover: Receive = Actor.emptyBehavior

    def receiveCommand: Receive = {
      case "Finish" => stopProcess()
    }

    def predicateArgument: Any = ctx.correlationId

    override def preStart(): Unit = {
      super.preStart()
      initializer.payload.poMsgProbe ! ("Started", self)
    }

    override def postStop(): Unit = {
      initializer.payload.poMsgProbe ! "Stopped"
      super.postStop()
    }
  }

  implicit val a = new ProcessActivator[MyPayload] {
    def propsFor(context: ProcessContext, initializer: Initializer[MyPayload])(): Props = Props(new MyProcess(context, initializer))
  }
}
