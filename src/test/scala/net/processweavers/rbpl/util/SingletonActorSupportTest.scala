package net.processweavers.rbpl.util

import java.util.concurrent.TimeUnit

import akka.actor.{ Actor, ActorSystem, Props }
import akka.pattern.pipe
import akka.pattern.ask
import akka.testkit.{ ImplicitSender, TestKit }
import akka.util.Timeout
import org.scalatest.{ BeforeAndAfterAll, WordSpecLike }
import org.scalatest.concurrent.{ PatienceConfiguration, ScalaFutures }
import org.scalatest.time.{ Milliseconds, Span }

class SingletonActorSupportTest extends TestKit(ActorSystem("as-SingletonActorSupportTest"))
  with WordSpecLike
  with ImplicitSender
  with BeforeAndAfterAll
  with ScalaFutures {

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  val VeryLongWait = PatienceConfiguration.Timeout(Span(40000, Milliseconds))

  "A SingletonActor" must {
    "retry resolving an actor with a very slow creation process" in {
      val fa1 = MySingletonActor()
      val fa2 = MySingletonActor()
      val fa3 = MySingletonActor()
      whenReady(fa1, VeryLongWait) { a =>
        a ! "Ping"
        system.log.info("Ready, a={}", a)
        whenReady(MySingletonActor(), VeryLongWait) { _ =>
          a ! "Ping"
          expectMsgType[Long]
          expectMsgType[Long]
        }
      }
    }

    "respond properly to 'tell' in withActor" in {
      implicit val t = Timeout(1000, TimeUnit.MILLISECONDS)
      MySingletonActor.withActor { a =>
        a ! "Ask"
      }
      expectMsg("Response")
    }

    "respond properly to 'ask' in withActor" in {
      implicit val t = Timeout(1000, TimeUnit.MILLISECONDS)
      MySingletonActor.withActor { a =>
        import system.dispatcher
        pipe((a ? "Ask").mapTo[String]) to self
      }
      expectMsg("Response")
    }
  }
}

object MySingletonActor extends SingletonActorSupport {
  val ACTORNAME = "WALTER"
  protected def newActorProps(implicit system: ActorSystem) = {
    Props(new Actor {
      val x: Long = (-1000000000L to 1000000L).foldLeft(1L) { case (a, b) => a + b }
      system.log.info("x={}", x)

      def receive = {
        case "Ping" =>
          system.log.info("Ping!")
          sender ! x
        case "Ask" => sender() ! "Response"
      }
    })
  }
}