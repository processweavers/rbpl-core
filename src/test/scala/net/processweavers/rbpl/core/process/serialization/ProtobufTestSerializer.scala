package net.processweavers.rbpl.core.process.serialization

import java.util.UUID

import akka.actor.{ ActorRef, ExtendedActorSystem }
import net.processweavers.rbpl.core.process.{ ProcessAndThenActionConsistencyTest, ProcessTest }
import net.processweavers.rbpl.core.process.testmodels._
import net.processweavers.rbpl.core.task.{ ResultId, TaskContext }
import net.processweavers.rbpl.util.ProcessFixtures.{ MyPayload, Task1 }

class ProtobufTestSerializer(system: ExtendedActorSystem) extends BaseProcessSerializer(system) {

  override val identifier: Int = 9100

  implicit def fromMyPayload(t: MyPayload): MyPayloadPersisted =
    MyPayloadPersisted(t.aString, trySerialize(t.poMsgProbe))
  implicit def fromMyPayloadPersisted(t: MyPayloadPersisted): MyPayload =
    MyPayload(t.aString, tryDeserializeAs[ActorRef](t.poMsgProbe))

  implicit def fromTask1Task1Payload(t: Task1.Task1Payload): Task1PayloadPersisted = Task1PayloadPersisted(t.a)
  implicit def fromTask1PayloadPersisted(t: Task1PayloadPersisted): Task1.Task1Payload = Task1.Task1Payload(t.s)

  implicit def fromProcessTestTask1MyPayload(t: ProcessTest.Task1.MyPayload): ProcessTest_Task1_PayloadPersisted = ProcessTest_Task1_PayloadPersisted(t.s, t.i)
  implicit def fromProcessTest_Task1_PayloadPersisted(t: ProcessTest_Task1_PayloadPersisted): ProcessTest.Task1.MyPayload = ProcessTest.Task1.MyPayload(t.s, t.i)

  implicit def fromProcessTestTask2MyPayload(t: ProcessTest.Task2.MyPayload): ProcessTest_Task2_MyPayloadPersisted = ProcessTest_Task2_MyPayloadPersisted(t.s)
  implicit def fromProcessTest_Task2_MyPayloadPersisted(t: ProcessTest_Task2_MyPayloadPersisted): ProcessTest.Task2.MyPayload = ProcessTest.Task2.MyPayload(t.s)

  implicit def fromProcessTestTask3MyPayload(t: ProcessTest.Task3.MyPayload): ProcessTest_Task3_MyPayloadPersisted = ProcessTest_Task3_MyPayloadPersisted(t.s)
  implicit def fromProcessTest_Task3_MyPayloadPersisted(t: ProcessTest_Task3_MyPayloadPersisted): ProcessTest.Task3.MyPayload = ProcessTest.Task3.MyPayload(t.s)

  implicit def fromProcessTestMyTaskResult1(t: ProcessTest.MyTaskResult1): ProcessTest_MyTaskResult1Persisted =
    ProcessTest_MyTaskResult1Persisted(t.name, trySerialize(t.taskContext), t.resultId.id.toString)
  implicit def fromProcessTest_MyTaskResult1Persisted(ser: ProcessTest_MyTaskResult1Persisted): ProcessTest.MyTaskResult1 =
    ProcessTest.MyTaskResult1(ser.name, tryDeserializeAs[TaskContext](ser.context), ResultId(UUID.fromString(ser.resultId)))

  implicit def fromProcessTestMyTaskResult2(t: ProcessTest.MyTaskResult2): ProcessTest_MyTaskResult2Persisted =
    ProcessTest_MyTaskResult2Persisted(t.name, t.i, trySerialize(t.taskContext), t.resultId.id.toString)
  implicit def fromProcessTest_MyTaskResult2Persisted(ser: ProcessTest_MyTaskResult2Persisted): ProcessTest.MyTaskResult2 =
    ProcessTest.MyTaskResult2(ser.name, ser.i, tryDeserializeAs[TaskContext](ser.context), ResultId(UUID.fromString(ser.resultId)))

  implicit def fromProcessAndThenActionConsistencyTest_Task0Payload(ser: ProcessAndThenActionConsistencyTest.Task0.Task0Payload): ProcessAndThenActionConsistencyTest_Task0PayloadPersisted =
    ProcessAndThenActionConsistencyTest_Task0PayloadPersisted(ser.a)
  implicit def fromProcessAndThenActionConsistencyTest_Task0PayloadPersisted(t: ProcessAndThenActionConsistencyTest_Task0PayloadPersisted): ProcessAndThenActionConsistencyTest.Task0.Task0Payload =
    ProcessAndThenActionConsistencyTest.Task0.Task0Payload(t.a)

  implicit def fromProcessAndThenActionConsistencyTest_Task0_Result0(ser: ProcessAndThenActionConsistencyTest.Task0.Result0): ProcessAndThenActionConsistencyTest_Task0_Result0Persisted =
    ProcessAndThenActionConsistencyTest_Task0_Result0Persisted(trySerialize(ser.taskContext), ser.resultId.id.toString)
  implicit def fromProcessAndThenActionConsistencyTest_Task0_Result0Persisted(t: ProcessAndThenActionConsistencyTest_Task0_Result0Persisted): ProcessAndThenActionConsistencyTest.Task0.Result0 = {
    ProcessAndThenActionConsistencyTest.Task0.Result0(tryDeserializeAs[TaskContext](t.context), ResultId(UUID.fromString(t.resultId)))
  }

  implicit def fromProcessAndThenActionConsistencyTest_Task1Payload(ser: ProcessAndThenActionConsistencyTest.Task1.Task1Payload): ProcessAndThenActionConsistencyTest_Task1PayloadPersisted =
    ProcessAndThenActionConsistencyTest_Task1PayloadPersisted(ser.a)
  implicit def fromProcessAndThenActionConsistencyTest_Task1PayloadPersisted(t: ProcessAndThenActionConsistencyTest_Task1PayloadPersisted): ProcessAndThenActionConsistencyTest.Task1.Task1Payload =
    ProcessAndThenActionConsistencyTest.Task1.Task1Payload(t.a)

  val ms: Map[String, PersistanceMapper[_, _]] = Map(
    classOf[net.processweavers.rbpl.util.ProcessFixtures.MyPayload].getName ->
      new PersistanceMapper[net.processweavers.rbpl.util.ProcessFixtures.MyPayload, net.processweavers.rbpl.core.process.testmodels.MyPayloadPersisted](net.processweavers.rbpl.core.process.testmodels.MyPayloadPersisted.parseFrom),
    classOf[Task1.Task1Payload].getName ->
      new PersistanceMapper[Task1.Task1Payload, net.processweavers.rbpl.core.process.testmodels.Task1PayloadPersisted](net.processweavers.rbpl.core.process.testmodels.Task1PayloadPersisted.parseFrom),
    classOf[ProcessTest.Task1.MyPayload].getName ->
      new PersistanceMapper[ProcessTest.Task1.MyPayload, ProcessTest_Task1_PayloadPersisted](ProcessTest_Task1_PayloadPersisted.parseFrom),
    classOf[ProcessTest.Task2.MyPayload].getName ->
      new PersistanceMapper[ProcessTest.Task2.MyPayload, ProcessTest_Task2_MyPayloadPersisted](ProcessTest_Task2_MyPayloadPersisted.parseFrom),
    classOf[ProcessTest.Task3.MyPayload].getName ->
      new PersistanceMapper[ProcessTest.Task3.MyPayload, ProcessTest_Task3_MyPayloadPersisted](ProcessTest_Task3_MyPayloadPersisted.parseFrom),
    classOf[ProcessTest.MyTaskResult1].getName ->
      new PersistanceMapper[ProcessTest.MyTaskResult1, ProcessTest_MyTaskResult1Persisted](ProcessTest_MyTaskResult1Persisted.parseFrom),
    classOf[ProcessTest.MyTaskResult2].getName ->
      new PersistanceMapper[ProcessTest.MyTaskResult2, ProcessTest_MyTaskResult2Persisted](ProcessTest_MyTaskResult2Persisted.parseFrom),
    classOf[ProcessAndThenActionConsistencyTest.Task0.Task0Payload].getName ->
      new PersistanceMapper[ProcessAndThenActionConsistencyTest.Task0.Task0Payload, ProcessAndThenActionConsistencyTest_Task0PayloadPersisted](ProcessAndThenActionConsistencyTest_Task0PayloadPersisted.parseFrom),
    classOf[ProcessAndThenActionConsistencyTest.Task0.Result0].getName ->
      new PersistanceMapper[ProcessAndThenActionConsistencyTest.Task0.Result0, ProcessAndThenActionConsistencyTest_Task0_Result0Persisted](ProcessAndThenActionConsistencyTest_Task0_Result0Persisted.parseFrom),
    classOf[ProcessAndThenActionConsistencyTest.Task1.Task1Payload].getName ->
      new PersistanceMapper[ProcessAndThenActionConsistencyTest.Task1.Task1Payload, ProcessAndThenActionConsistencyTest_Task1PayloadPersisted](ProcessAndThenActionConsistencyTest_Task1PayloadPersisted.parseFrom))

}
