package net.processweavers.rbpl.core.process

import java.util.concurrent.TimeUnit

import akka.actor.ActorRef
import akka.testkit.TestProbe
import net.processweavers.rbpl.core.Initializer
import net.processweavers.rbpl.core.process.ProcessRepo.{ FindWithCorrelationId, FindWithPredicate, FinderResult, FinderResults, NewProcessStarted, StartNewProcess }
import net.processweavers.rbpl.core.process.ProcessRepo.{ FinderResults, NewProcessStarted }
import org.scalatest.BeforeAndAfterEach
import net.processweavers.testsupport.PersistentActorTestKit

import scala.concurrent.duration.FiniteDuration

class ProcessRepoTest extends PersistentActorTestKit("as-process-test") with BeforeAndAfterEach {

  import net.processweavers.rbpl.util.ProcessFixtures._

  "A Processes repository" must {

    "start new Process instances" in {

      val poProbe = TestProbe()
      val correlationId = CorrelationId("1")
      val myInit = MyPayload("Bla", poProbe.ref)

      whenReady(ProcessRepo()) { pRepo =>
        pRepo ! StartNewProcess(correlationId, Initializer("bla", myInit))
        expectMsgType[NewProcessStarted].ctx.correlationId shouldBe correlationId
        val started = poProbe.expectMsgType[(String, ActorRef)]
        started._1 shouldBe "Started"

        pRepo ! FindWithCorrelationId(correlationId)
        expectMsgPF() {
          case FinderResult(Some((ProcessContext(ProcessId(_), `correlationId`, ProcessName(myInit.name), _), started._2))) => true
          case _ => false
        }

        pRepo ! FindWithCorrelationId(CorrelationId("-1"))
        expectMsg(FinderResult(None))

        gracefullyTerminatUserActor(ProcessRepo.ACTORNAME)
        poProbe.expectMsg("Stopped") // repo shutdown => all po shutdown

        whenReady(ProcessRepo()) { rehydratedRepo =>
          val restarted = poProbe.expectMsgType[(String, ActorRef)]
          restarted._1 shouldBe "Started"

          rehydratedRepo ! FindWithCorrelationId(correlationId)
          expectMsgPF() {
            case FinderResult(Some((ProcessContext(ProcessId(_), `correlationId`, ProcessName(myInit.name), _), restarted._2))) => true
            case _ => false
          }

          restarted._2 ! "Finish"
          poProbe.expectMsg("Stopped")

          gracefullyTerminatUserActor(ProcessRepo.ACTORNAME)
          poProbe.expectNoMessage(FiniteDuration(1, TimeUnit.SECONDS))

          val secondRehydratedRepo = ProcessRepo()
          poProbe.expectNoMessage(FiniteDuration(1, TimeUnit.SECONDS))
        }
      }
    }

    "locate process instances by predicate" in {
      val poProbe = TestProbe()

      whenReady(ProcessRepo()) { pRepo =>
        val correlationId1 = CorrelationId("1")
        pRepo ! StartNewProcess(correlationId1, Initializer("bla", MyPayload("1", poProbe.ref)))
        expectMsgType[NewProcessStarted].ctx.correlationId shouldBe correlationId1

        val ref1 = poProbe.expectMsgType[(String, ActorRef)]._2

        val correlationId2 = CorrelationId("2")
        val initializer = Initializer("bla", MyPayload("2", poProbe.ref))
        pRepo ! StartNewProcess(correlationId2, initializer)
        val nps2 = expectMsgType[NewProcessStarted]
        nps2.ctx.correlationId shouldBe correlationId2
        val ref2 = poProbe.expectMsgType[(String, ActorRef)]._2

        pRepo ! FindWithPredicate({
          case corrId: CorrelationId => corrId == correlationId2
        })
        expectMsgType[FinderResults].maybeProcesses should contain(nps2.ctx, ref2)

        pRepo ! FindWithPredicate({ case corrId: String => corrId == "SSS" })
        expectMsgType[FinderResults].maybeProcesses shouldBe empty

      }
    }
  }
}
