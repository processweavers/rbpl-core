package net.processweavers.rbpl.core.process

import java.util.UUID
import java.util.concurrent.TimeUnit

import akka.actor.{ Actor, ActorRef, Props }
import akka.testkit.TestProbe
import net.processweavers.rbpl.core.Initializer
import net.processweavers.rbpl.core.process.AndThenAPI.{ StartNewTask, StopCurrentTask }
import net.processweavers.rbpl.core.task._
import net.processweavers.rbpl.core.testsupport.{ ProcessTestScope, ProcessTestable }
import net.processweavers.testsupport.PersistentActorTestKit
import shapeless.HNil

import scala.concurrent.duration.{ Duration, FiniteDuration }

class ProcessAndThenActionConsistencyTest extends PersistentActorTestKit("as-process-test") {

  def ok(): Unit = {}

  def failure(): Unit = throw new RuntimeException("Peng!!")

  import ProcessAndThenActionConsistencyTest._

  val shortWaitForMessage = Duration(500, TimeUnit.MILLISECONDS)

  "A Process" should {

    "persist result and crash in the result handler => restart task of persisted result because StopCurrentTask was not executed" in new ProcessTestScope {
      /*
        The result is persisted but the current task not stopped and will be restarted.
         - the task needs to behave idempotently, e.g. by using persistence if a side effect is performed
         - the state update of the result on the process must also be idempotent since there would be more then one in such a situation
       */

      val actorName = classOf[ProcessAndThenActionConsistencyTest.CrasherProcess].getSimpleName + UUID.randomUUID
      val p = system.actorOf(Props(new CrasherFixture(processProbe.ref, TestProbe().ref, processContext, Initializer(actorName, MyPayload("a")), failure)), actorName)

      val t0 = expectTaskStarted("T0-1")
      p ! Task0.Result0(t0.context, ResultId.create)

      expectTaskStarted("T0-1")
    }

    "persist result and crash in one of the the started actors" in new ProcessTestScope {

      /*
        If the next task fails to start then there are two possibilities:
          - exception during actor initialization => results in an ActorInitializationException and this is NOT handled (simply restarting it will result in the same error)
          - exception after proper initialization => results in a restart by the default supervision strategy

        The process registry has TaskStarted in any case and will restart it on its restart.

        TODO: Use BackingOff instead of always restart strategy to reduce load and finally react end terminate the process with a failure
       */

      val actorName = classOf[ProcessAndThenActionConsistencyTest.CrasherProcess].getSimpleName + UUID.randomUUID
      val p = system.actorOf(Props(new CrasherFixture(processProbe.ref, TestProbe().ref, processContext, Initializer(actorName, MyPayload("a")), ok)), actorName)

      val t0 = expectTaskStarted("T0-1")
      p ! Task0.Result0(t0.context, ResultId.create)
      expectTaskStopped(t0)
      expectTaskStarted("T1")
      expectTaskStarted("T0-2")

      gracefullyTerminatUserActor(actorName)
      val pNew = system.actorOf(Props(new CrasherFixture(processProbe.ref, TestProbe().ref, processContext, Initializer(actorName, MyPayload("a")), ok)), actorName)

      expectTaskStarted("T1")
      expectTaskStarted("T0-2")
    }

    "persist result and crash in one of the AndThenActions themself" in new ProcessTestScope {

      /*
        Simulates a (VM) crash before stopping the current task
        - process actor will be restarted immediately process the "atomic" ProcessAll actions
          - either all actions will be replayed
          - or no action is replayed and the task restarted => idempotency handling.
       */

      override val waitForMessage: FiniteDuration = Duration(500, TimeUnit.MILLISECONDS)

      val actorName = classOf[ProcessAndThenActionConsistencyTest.CrasherProcess].getSimpleName + UUID.randomUUID
      val p = system.actorOf(Props(new CrasherFixture(processProbe.ref, TestProbe().ref, processContext, Initializer(actorName, MyPayload("crashAfterStop")), ok)), actorName)

      val t0 = expectTaskStarted("T0-1")
      p ! Task0.Result0(t0.context, ResultId.create)

      // T0-1 stopped and T1, T0-2 running
      //      expectTaskStopped(t0) => does not work (no notifcation in test), but task is stopped, see process dump
      expectTaskStarted("T1")
      expectTaskStarted("T0-2")

      dumpHistory(p, "-constistenty")

    }
  }
}

object ProcessAndThenActionConsistencyTest {

  class CrasherFixture(probe: ActorRef, genericTaskProbe: ActorRef, ctx: ProcessContext, initializer: Initializer[MyPayload], handler: () => Unit) extends ProcessAndThenActionConsistencyTest.CrasherProcess(probe, genericTaskProbe, ctx, initializer) {
    override def handleResultHook(): Unit = handler()
  }

  case object EmptyPayload

  class Task0(val descriptor: TaskDescriptor, i: Initializer[Task0.Task0Payload]) extends Actor with Task {
    def receive: Receive = Actor.emptyBehavior
  }

  object Task0 {

    case class Result0(taskContext: TaskContext, resultId: ResultId) extends TaskResult
    case class Task0Payload(a: String)

    implicit val activator0 = new TaskActivator[Task0Payload] {
      def propsFor(processContext: ProcessContext, taskContext: TaskContext, i: Initializer[Task0Payload])(): Props =
        Props(new Task0(TaskDescriptor(i.name, processContext, taskContext, i.description), i))
    }
  }

  class Task1(val descriptor: TaskDescriptor, i: Initializer[Task1.Task1Payload]) extends Actor with Task {

    def receive: Receive = {
      case Initialize =>
        throw new Exception("Task1 - Initialize")
    }
  }

  object Task1 {

    case class Task1Payload(a: String)
    case class Result1(taskContext: TaskContext, resultId: ResultId) extends TaskResult

    implicit val activator1 = new TaskActivator[Task1Payload] {
      def propsFor(processContext: ProcessContext, taskContext: TaskContext, i: Initializer[Task1Payload])(): Props =
        Props(new Task1(TaskDescriptor(i.name, processContext, taskContext, i.description), i))
    }
  }

  class Task2(val descriptor: TaskDescriptor, i: Initializer[Task2.Task2Payload]) extends Actor with Task {
    def receive: Receive = Actor.emptyBehavior
  }

  object Task2 {

    case class Task2Payload(a: String)

    implicit val activator2 = new TaskActivator[Task2Payload] {
      def propsFor(processContext: ProcessContext, taskContext: TaskContext, i: Initializer[Task2Payload])(): Props =
        Props(new Task2(TaskDescriptor(i.name, processContext, taskContext, i.description), i))
    }
  }

  case class MyPayload(aString: String) {
    val name: String = classOf[CrasherProcess].getName
  }

  class CrasherProcess(val probe: ActorRef, val genericTaskProbe: ActorRef, val ctx: ProcessContext, val initializer: Initializer[MyPayload]) extends BaseProcess(Initializer("T0-1", Task0.Task0Payload("T0-1")) :: HNil) with ProcessTestable {

    override protected def newTaskActor(s: (TaskDescriptor, Props)): ActorRef =
      context.actorOf(s._2, s"${s._1.name}-${s._1.context.taskId.id}")

    override protected def stopTask(tid: TaskId): Unit = {
      if (initializer.payload.aString == "crashAfterStop")
        throw new Exception("crashAfterStop")
      super.stopTask(tid)
    }

    var state: Int = 0

    def receiveRecover: Receive = {
      case m =>
    }

    def handleResultHook(): Unit = {}

    def receiveCommand: Receive = {
      case r: Task0.Result0 =>
        persistAndHandleResult(r) { res =>
          handleResultHook()
        } andThen (
          StopCurrentTask(),
          StartNewTask("T1", Initializer("T1", Task1.Task1Payload("c"))),
          StartNewTask("T0-2", Initializer("T0-2", Task0.Task0Payload("T0-2"))))

      case r: Task1.Result1 =>
        sender ! "OK"
    }

    def predicateArgument: Any = ctx.correlationId
  }

  //  implicit val a = new ProcessActivator[MyPayload] {
  //    def propsFor(context: ProcessContext, initializer: Initializer[MyPayload])(): Props = Props(new CrasherProcess(context, initializer))
  //  }
}

