package net.processweavers.rbpl.core.process.serialization

import akka.actor.ActorSystem
import akka.remote.ContainerFormats.ActorRef
import akka.serialization.{ Serialization, SerializationExtension }
import akka.testkit.{ TestKit, TestProbe }
import net.processweavers.rbpl.core.{ Initializer, task }
import net.processweavers.rbpl.core.process.ProcessPrivateApi.{ StopTask, StartTask }
import net.processweavers.rbpl.core.process._
import net.processweavers.rbpl.core.task.{ ResultId, TaskContext, TaskId }
import org.scalatest.{ BeforeAndAfterAll, Matchers, WordSpecLike }

class ProcessProtoBufSerializerTest extends TestKit(ActorSystem())
  with WordSpecLike
  with Matchers
  with BeforeAndAfterAll
  with SerializationSupport {

  lazy val serializerExt: Serialization = SerializationExtension(system)

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "A ProcessProtoBufSerializer" must {
    "serialize a String" in {
      val s = "Walter"
      tryDeserializeAs[String](trySerialize(s)) shouldBe s
    }

    "serialize a TaskContext" in {
      val t = TaskContext(TaskId.create, None, None)
      tryDeserializeAs[TaskContext](trySerialize(t)) shouldBe t
    }

    "serialize a StopTask" in {
      val t = StopTask(TaskId.create, "")
      tryDeserializeAs[StopTask](trySerialize(t)) shouldBe t
    }

    "serialize a ProcessContext" in {
      val t = ProcessContext(
        ProcessId.create,
        CorrelationId("correlationId"),
        ProcessName("processName"),
        "desc")
      tryDeserializeAs[ProcessContext](trySerialize(t)) shouldBe t
    }

    "serialize a ProcessRepoPrivateApi.ProcessStarted" in {
      val t = ProcessRepoPrivateApi.ProcessStarted(
        Initializer("name", net.processweavers.rbpl.util.ProcessFixtures.MyPayload("", TestProbe().ref)),
        "initializerName",
        "activatorName",
        ProcessContext(ProcessId.create, CorrelationId("corrId"), ProcessName("name")))
      tryDeserializeAs[ProcessRepoPrivateApi.ProcessStarted[_]](trySerialize(t)) shouldBe t
    }

    "serialize a TaskStarted" in {
      val t = StartTask(
        Initializer("name", net.processweavers.rbpl.util.ProcessFixtures.MyPayload("", TestProbe().ref)),
        "initializerName",
        "activatorName",
        TaskContext(TaskId.create, Some(ResultId.create), Some("xxx")))
      tryDeserializeAs[StartTask[_]](trySerialize(t)) shouldBe t
    }

    "serialize a ProcessAndThenActionConsistencyTest.Task0.Result0" in {
      val t = ProcessAndThenActionConsistencyTest.Task0.Result0(
        TaskContext(TaskId.create, Some(ResultId.create), Some("xxx")),
        ResultId.create)
      tryDeserializeAs[ProcessAndThenActionConsistencyTest.Task0.Result0](trySerialize(t)) shouldBe t
    }
  }

}

