package net.processweavers.rbpl.core.process

import java.io.{ BufferedWriter, File, FileWriter }
import java.util.UUID
import java.util.concurrent.TimeUnit

import akka.actor.{ Actor, ActorRef, Props }
import akka.testkit.TestProbe
import net.processweavers.rbpl.core.Initializer
import net.processweavers.rbpl.core.process.ProcessPrivateApi._
import net.processweavers.rbpl.core.task.TimerTask.TimerExpired
import net.processweavers.rbpl.core.task._
import shapeless._
import net.processweavers.rbpl.util._
import net.processweavers.testsupport.PersistentActorTestKit

import scala.concurrent.duration.FiniteDuration

class ProcessTest extends PersistentActorTestKit("as-process-test") {

  import ProcessTest._

  "A Process" should {
    "create all initial tasks when created" in {
      val pMsgProbe = TestProbe()
      val taskMsgProbe = TestProbe()
      val resultProbe = TestProbe()
      val p = system.actorOf(Props(new MyProcess(ProcessContext(ProcessId.create, CorrelationId(""), ProcessName("Test")), pMsgProbe.ref, taskMsgProbe.ref, resultProbe.ref)), classOf[MyProcess].getSimpleName + UUID.randomUUID)
      taskMsgProbe.expectMsgAllOf("Started-1", "Started-2")
      pMsgProbe.expectMsgType[Test_StartTask]
      pMsgProbe.expectMsgType[Test_StartTask]
      p ! "allActiveTasks"
      val ats = pMsgProbe.expectMsgType[Map[TaskId, (String, ActorRef)]]
      ats should have size 2
      ats.map(_._2._1) should contain allOf (Task1.taskName, Task2.taskName)
    }

    "restart not successfully started initial tasks" in {
      val initializers = Seq(
        StartTask(null, null, null, TaskContext(TaskId.create, None, None)),
        StartTask(null, null, null, TaskContext(TaskId.create, None, None)),
        StartTask(null, null, null, TaskContext(TaskId.create, None, None)))
      val started = Map(initializers.head.taskContext.taskId -> null)

      InitialTasksToStart.findNotStarted(initializers, started) should contain allOf (initializers(1), initializers(2))
    }

    "persist result and continue with action" in {
      val pMsgProbe = TestProbe()
      val taskMsgProbe = TestProbe()
      val resultProbe = TestProbe()
      val p = system.actorOf(Props(new MyProcess(ProcessContext(ProcessId.create, CorrelationId(""), ProcessName("Test")), pMsgProbe.ref, taskMsgProbe.ref, resultProbe.ref)), classOf[MyProcess].getSimpleName + UUID.randomUUID)
      taskMsgProbe.expectMsgAllOf("Started-1", "Started-2")
      val t1 = pMsgProbe.expectMsgType[Test_StartTask]
      val t2 = pMsgProbe.expectMsgType[Test_StartTask]

      val result = MyTaskResult1("result", t1.taskDescriptor.context, ResultId.create)
      p ! result
      resultProbe.expectMsg(result)
      pMsgProbe.expectMsg(Test_StopTask(result.taskContext.taskId))
      val td1 = pMsgProbe.expectMsgType[Test_StartTask].taskDescriptor
      td1.name shouldBe Task3.taskName
      val td2 = pMsgProbe.expectMsgType[Test_StartTask].taskDescriptor
      td2.name shouldBe Task3.taskName
      td1.context.taskId should not be td2.context.taskId
    }

    "recover persisted result and continue with action" in {
      val pMsgProbe = TestProbe()
      val reveivedPoMsgProbe = TestProbe()

      val taskMsgProbe = TestProbe()
      val reveivedTaskMsgProbe = TestProbe()
      val resultProbe = TestProbe()
      val reveivedResultProbe = TestProbe()

      val actorName = classOf[MyProcess].getSimpleName + UUID.randomUUID
      val processId = ProcessId.create
      val p = system.actorOf(Props(new MyProcess(ProcessContext(processId, CorrelationId(""), ProcessName("Test")), pMsgProbe.ref, taskMsgProbe.ref, resultProbe.ref)), actorName)
      taskMsgProbe.expectMsgAllOf("Started-1", "Started-2")
      val t1 = pMsgProbe.expectMsgType[Test_StartTask]
      val t2 = pMsgProbe.expectMsgType[Test_StartTask]

      val result = MyTaskResult1("result-1", t1.taskDescriptor.context, ResultId.create)
      p ! result

      // Expect task1 to be stopped
      resultProbe.expectMsg(result)
      pMsgProbe.expectMsg(Test_StopTask(result.taskContext.taskId))
      taskMsgProbe.expectMsg("Stopped-1")

      // Expect 2 times Task3 to be started
      val td1 = pMsgProbe.expectMsgType[Test_StartTask].taskDescriptor
      td1.name shouldBe Task3.taskName
      val td2 = pMsgProbe.expectMsgType[Test_StartTask].taskDescriptor
      td2.name shouldBe Task3.taskName
      td1.context.taskId should not be td2.context.taskId
      taskMsgProbe.expectMsgAllOf("Started-3", "Started-3")

      dumpHistory(p, "dotHistory-1.dot")

      gracefullyTerminatUserActor(actorName)

      val revivedProcess = system.actorOf(Props(new MyProcess(ProcessContext(processId, CorrelationId(""), ProcessName("Test")), reveivedPoMsgProbe.ref, reveivedTaskMsgProbe.ref, reveivedResultProbe.ref)), actorName)
      reveivedTaskMsgProbe.expectMsgAllOf("Started-2", "Started-3", "Started-3")
      reveivedPoMsgProbe.expectMsgType[Test_StartTask]

      val td3 = reveivedPoMsgProbe.expectMsgType[Test_StartTask].taskDescriptor
      td3.name shouldBe Task3.taskName
      val td4 = reveivedPoMsgProbe.expectMsgType[Test_StartTask].taskDescriptor
      td4.name shouldBe Task3.taskName
      td3.context.taskId should not be td4.context.taskId

      dumpHistory(revivedProcess, "dotHistory-2.dot")

      val result2 = MyTaskResult2("result-3", 3, td2.context, ResultId.create)
      revivedProcess ! result2

      // Expect task3 to be stopped
      reveivedResultProbe.expectMsg(result2)
      reveivedPoMsgProbe.expectMsg(Test_StopTask(result2.taskContext.taskId))
      reveivedTaskMsgProbe.expectMsg("Stopped-3")

      dumpHistory(revivedProcess, "dotHistory-3.dot")
    }

    "handle timers properly" in {
      val pMsgProbe = TestProbe()

      val taskMsgProbe = TestProbe()

      val actorName = classOf[MyProcess].getSimpleName + UUID.randomUUID
      val processId = ProcessId.create
      val p = system.actorOf(Props(new MyTimerTestProcess(ProcessContext(processId, CorrelationId(""), ProcessName("MyTimerTestProcess")), pMsgProbe.ref, taskMsgProbe.ref)), actorName)
      taskMsgProbe.expectMsg("Started-1")
      val t1 = pMsgProbe.expectMsgType[Test_StartTask]

      val result = MyTaskResult1("result-1", t1.taskDescriptor.context, ResultId.create)
      p ! result

      // Expect task1 to be stopped
      taskMsgProbe.expectMsg(result)
      pMsgProbe.expectMsg(Test_StopTask(result.taskContext.taskId))
      taskMsgProbe.expectMsg("Stopped-1")

      // Expect TimerTask to be started
      val td1 = pMsgProbe.expectMsgType[Test_StartTask].taskDescriptor
      td1.name shouldBe s"TimerTask-${MyTimerTestProcess.MyTimerId}"

      val te = taskMsgProbe.expectMsgType[TimerExpired]
      te.timerId shouldBe MyTimerTestProcess.MyTimerId

      pMsgProbe.expectMsg(Test_StopTask(te.taskContext.taskId))
    }
  }

  def dumpHistory(p: ActorRef, filename: String): Unit = {
    p ! Process.GetHistory
    val history = expectMsgType[ProcessHistoryRecorder.History]
    val file = new File(filename)
    using(new BufferedWriter(new FileWriter(file))) { bw =>
      bw.write(HistoryDotRenderer.toDot(history))
    }
    system.log.error(s"RESULT LOG => file://${HistoryDotRenderer.toHtml(history, filenamePostFix = s"-$filename")}")
  }
}

object ProcessTest {
  class Task1(val descriptor: TaskDescriptor, i: Initializer[Task1.MyPayload]) extends Actor with Task {

    def receive: Receive = Actor.emptyBehavior

    override def preStart(): Unit = {
      super.preStart()
      context.parent ! "Started-1"
    }

    override def postStop(): Unit = {
      context.parent ! "Stopped-1"
      super.postStop()
    }

  }

  object Task1 {

    val taskName = "Task1"

    case class MyPayload(s: String, i: Int)

    implicit val activator1 = new TaskActivator[MyPayload] {
      def propsFor(processContext: ProcessContext, taskContext: TaskContext, i: Initializer[MyPayload])(): Props = Props(new Task1(TaskDescriptor(i.name, processContext, taskContext, i.description), i))
    }
  }

  class Task2(val descriptor: TaskDescriptor, i: Initializer[Task2.MyPayload]) extends Actor with Task {
    def receive: Receive = Actor.emptyBehavior

    override def preStart(): Unit = {
      super.preStart()
      context.parent ! "Started-2"
    }

    override def postStop(): Unit = {
      context.parent ! "Stopped-2"
      super.postStop()
    }
  }

  object Task2 {

    val taskName = "Task2"

    case class MyPayload(s: String)

    implicit val activator2 = new TaskActivator[Task2.MyPayload] {
      def propsFor(processContext: ProcessContext, taskContext: TaskContext, i: Initializer[Task2.MyPayload])(): Props = Props(new Task2(TaskDescriptor(i.name, processContext, taskContext, i.description), i))
    }
  }

  class Task3(val descriptor: TaskDescriptor, i: Initializer[Task3.MyPayload]) extends Actor with Task {
    def receive: Receive = Actor.emptyBehavior

    override def preStart(): Unit = {
      super.preStart()
      context.parent ! "Started-3"
    }

    override def postStop(): Unit = {
      context.parent ! "Stopped-3"
      super.postStop()
    }
  }

  object Task3 {

    val taskName = "Task3"

    case class MyPayload(s: String)

    implicit val activator3 = new TaskActivator[Task3.MyPayload] {
      def propsFor(processContext: ProcessContext, taskContext: TaskContext, i: Initializer[Task3.MyPayload])(): Props = Props(new Task3(TaskDescriptor(i.name, processContext, taskContext, i.description), i))
    }
  }

  case class MyTaskResult1(name: String, taskContext: TaskContext, resultId: ResultId) extends TaskResult
  case class MyTaskResult2(name: String, i: Int, taskContext: TaskContext, resultId: ResultId) extends TaskResult
  case class Test_StartTask(taskDescriptor: TaskDescriptor)
  case class Test_StopTask(taskId: TaskId)

  class MyProcess(val ctx: ProcessContext, val poMsgProbe: ActorRef, val taskMsgProbe: ActorRef, val resultProbe: ActorRef) extends BaseProcess(Initializer(Task1.taskName, Task1.MyPayload("carsten", 1)) :: Initializer(Task2.taskName, Task2.MyPayload("ank")) :: HNil, Some(Initializer("MyProcess", (poMsgProbe, taskMsgProbe)))) {

    import AndThenAPI._

    def predicateArgument: Any = None

    override protected def stopTask(tid: TaskId): Unit = {
      super.stopTask(tid)
      poMsgProbe ! Test_StopTask(tid)
    }

    override protected def startTask(s: (TaskDescriptor, Props)): Unit = {
      super.startTask(s)
      poMsgProbe ! Test_StartTask(s._1)
    }

    def receiveRecover: Receive = Actor.emptyBehavior

    def receiveCommand: Receive = {
      case "allActiveTasks" => poMsgProbe ! allActiveTasks
      case r: MyTaskResult1 =>
        persistAndHandleResult(r) { res =>
          resultProbe ! res
        } andThen (
          StopCurrentTask(),
          StartNewTask("first new T3", Initializer(Task3.taskName, Task3.MyPayload("carsten"))),
          StartNewTask("second new T3", Initializer(Task3.taskName, Task3.MyPayload("carsten"))))
      case r: MyTaskResult2 =>
        persistAndHandleResult(r) { res =>
          resultProbe ! res
        } andThen
          StopCurrentTask()

      case m => taskMsgProbe ! m
    }
  }

  class MyTimerTestProcess(val ctx: ProcessContext, val poMsgProbe: ActorRef, val taskMsgProbe: ActorRef) extends BaseProcess(Initializer(Task1.taskName, Task1.MyPayload("carsten", 1)) :: HNil) {

    import AndThenAPI._
    import MyTimerTestProcess._

    def predicateArgument: Any = None

    override protected def stopTask(tid: TaskId): Unit = {
      super.stopTask(tid)
      poMsgProbe ! Test_StopTask(tid)
    }

    override protected def startTask(s: (TaskDescriptor, Props)): Unit = {
      super.startTask(s)
      poMsgProbe ! Test_StartTask(s._1)
    }

    def receiveRecover: Receive = Actor.emptyBehavior

    def receiveCommand: Receive = {
      case "allActiveTasks" => poMsgProbe ! allActiveTasks
      case r: MyTaskResult1 =>
        persistAndHandleResult(r) { res =>
          taskMsgProbe ! res
        } andThen (
          StopCurrentTask(),
          StartTimer(MyTimerId, FiniteDuration(500, TimeUnit.MILLISECONDS)))
      case te: TimerExpired =>
        persistAndHandleResult(te) { res =>
          taskMsgProbe ! res
        } andThen (
          StopCurrentTimer())
      case m => taskMsgProbe ! m
    }
  }

  object MyTimerTestProcess {
    val MyTimerId = "MyTimer"
  }

}
