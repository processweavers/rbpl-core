package net.processweavers.rbpl.core.task

import java.time.Instant
import java.util.concurrent.TimeUnit

import akka.actor.Props
import net.processweavers.rbpl.core.task.TimerTask.TimerExpired
import net.processweavers.rbpl.util.TestFixtures
import net.processweavers.testsupport.PersistentActorTestKit

import scala.concurrent.duration.FiniteDuration

class TimerTaskTest extends PersistentActorTestKit("as-timer-task-test") with TestFixtures {

  val timerId = "TimerId"

  "A TimerTask" must {

    "send a TimerExpired message back to its parent not earlier than the specified duration" in {
      val td = testTaskDescriptor()
      system.actorOf(Props(new TimerTask(td, FiniteDuration(500, TimeUnit.MILLISECONDS), timerId) {
        override protected def parentProcess = testActor
        override private[core] def scheduleOnce(d: FiniteDuration) = {
          testActor ! "Scheduled"
          super.scheduleOnce(d)
        }
      }), "Timer-Task-1")
      expectMsg("Scheduled")
      expectNoMessage(FiniteDuration(500, TimeUnit.MILLISECONDS))
      expectMsgType[TimerExpired](FiniteDuration(50, TimeUnit.MILLISECONDS)).timerId shouldBe timerId
    }

    "recover with non-expired timer" in {
      val td = testTaskDescriptor()
      val tt = system.actorOf(Props(new TimerTask(td, FiniteDuration(2000, TimeUnit.MILLISECONDS), timerId) {
        override protected def parentProcess = testActor
        override private[core] def scheduleOnce(d: FiniteDuration) = {
          testActor ! "Scheduled"
          super.scheduleOnce(d)
        }
      }), "Timer-Task-2")
      expectMsg("Scheduled")
      gracefullyTerminateActor(tt.path.toString)

      system.log.info("Recreating TimerTask")
      val tt2 = system.actorOf(Props(new TimerTask(td, FiniteDuration(2000, TimeUnit.MILLISECONDS), timerId) {
        override protected def parentProcess = testActor
        override private[core] def isExpired(i: Instant) = false
        override private[core] def scheduleOnce(d: FiniteDuration) = {
          testActor ! "Scheduled"
          super.scheduleOnce(d)
        }
      }), "Timer-Task-2")
      expectMsg("Scheduled")
      expectMsgType[TimerExpired].timerId shouldBe timerId
    }

    "recover with expired timer" in {
      val td = testTaskDescriptor()
      val tt = system.actorOf(Props(new TimerTask(td, FiniteDuration(2000, TimeUnit.MILLISECONDS), timerId) {
        override protected def parentProcess = testActor
        override private[core] def scheduleOnce(d: FiniteDuration) = {
          testActor ! "Scheduled"
          super.scheduleOnce(d)
        }
      }), "Timer-Task-3")
      expectMsg("Scheduled")
      gracefullyTerminateActor(tt.path.toString)

      system.log.info("Recreating TimerTask")
      val tt2 = system.actorOf(Props(new TimerTask(td, FiniteDuration(2000, TimeUnit.MILLISECONDS), timerId) {
        override protected def parentProcess = testActor
        override private[core] def isExpired(i: Instant) = true
        override private[core] def scheduleOnce(d: FiniteDuration) = {
          testActor ! "Scheduled"
          super.scheduleOnce(d)
        }
      }), "Timer-Task-3")
      expectMsgType[TimerExpired].timerId shouldBe timerId
    }
  }
}

