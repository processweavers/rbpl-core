package net.processweavers.rbpl

package object util {

  /**
   * Common definitions for location awareness
   */
  trait LocationAwareness {

    val defaultDomainName = "dummy.test"

    def actualDomainName: String

    def replacePlaceHolder(s: String): String =
      s.replace(defaultDomainName, actualDomainName)

  }

  def using[A <: { def close(): Unit }, B](param: A)(f: A => B): B =
    try { f(param) } finally { param.close() }

}