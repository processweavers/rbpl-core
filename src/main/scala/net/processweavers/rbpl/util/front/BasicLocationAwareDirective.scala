package net.processweavers.rbpl.util.front

import akka.http.scaladsl.model.HttpEntity
import akka.http.scaladsl.server.Directive0
import akka.http.scaladsl.server.directives.BasicDirectives
import akka.stream.scaladsl.Framing
import akka.util.ByteString
import net.processweavers.rbpl.util.LocationAwareness

trait BasicLocationAwareDirective extends BasicDirectives with LocationAwareness {

  def actualDomainName: String

  def withLocationAwareDomain: Directive0 =
    mapResponseEntity {
      case entity @ HttpEntity.Default(ct, length, data) =>
        val replaced = data
          .via(Framing.delimiter(ByteString("\n"), Int.MaxValue))
          .map(line => ByteString(replacePlaceHolder(line.utf8String)))
          .intersperse(ByteString("\n"))
        HttpEntity.CloseDelimited(ct, replaced)
      case entity @ HttpEntity.Strict(ct, data) =>
        HttpEntity.Strict(ct, ByteString(replacePlaceHolder(data.utf8String)))
      case entity => throw new RuntimeException(s"Invalid http entity type $entity")
    }

}
