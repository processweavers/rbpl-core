package net.processweavers.rbpl.util

import java.util.UUID
import java.util.concurrent.TimeUnit

import akka.actor.{ Actor, ActorIdentity, ActorNotFound, ActorRef, ActorSystem, Identify, InvalidActorNameException, Props }

import scala.concurrent.duration.FiniteDuration
import scala.concurrent.{ Future, Promise }
import scala.util.{ Failure, Success, Try }

trait SingletonActorSupport {

  class RetryingResolverCreator(p: Promise[ActorRef]) extends Actor {

    case object Resolve
    case object Create

    val reqId = UUID.randomUUID()
    val maxRetries = 10

    var retryCount = 0

    import context.dispatcher

    def selection = context.system.actorSelection(s"/user/$ACTORNAME")

    def completeWithSuccess(ref: ActorRef) = {
      p.success(ref)
      context.stop(self)
    }

    def completeWithFailure(t: Throwable) = {
      p.failure(t)
      context.stop(self)
    }

    def receive = tryResolveExisting

    def tryResolveExisting: Receive = {
      case Resolve =>
        context.system.log.debug("tryResolveExisting({})", reqId)
        selection.tell(Identify(1), self)

      case ActorIdentity(1, Some(ref)) =>
        context.system.log.debug("tryResolveExisting({}) - identified", reqId)
        completeWithSuccess(ref)

      case _: ActorIdentity =>
        context.system.log.debug("tryResolveExisting({}) - not identified, request creation", reqId)
        context.become(tryCreateNewActor)
        self ! Create
    }

    def tryCreateNewActor: Receive = {
      case Create =>
        Try {
          import context.system
          system.actorOf(newActorProps, ACTORNAME)
        } match {
          case Success(a) =>
            context.system.log.debug("tryCreateNewActor({}) - new actor created", reqId)
            completeWithSuccess(a)
          case Failure(_: InvalidActorNameException) =>
            context.system.log.debug("tryCreateNewActor({}) - actor exists, retry resolve", reqId)
            self ! Resolve
          case Failure(t) =>
            completeWithFailure(t)
        }

      case Resolve =>
        selection.tell(Identify(1), self)

      case ActorIdentity(1, Some(ref)) =>
        context.system.log.debug("tryCreateNewActor({}) - existing actor resolved", reqId)
        completeWithSuccess(ref)

      case ai: ActorIdentity =>
        context.system.log.debug("tryCreateNewActor({}) - resolve failed: {}", reqId, ai)
        if (retryCount < maxRetries) {
          retryCount = retryCount + 1
          context.system.log.debug("tryCreateNewActor({}) - retry resolve, retryCount={}", reqId, retryCount)
          context.system.scheduler.scheduleOnce(FiniteDuration(100, TimeUnit.MILLISECONDS), self, Resolve)
        } else {
          completeWithFailure(ActorNotFound(selection))
        }
    }

    override def preStart(): Unit = {
      super.preStart()
      self ! Resolve
    }
  }

  val ACTORNAME: String

  protected def newActorProps(implicit system: ActorSystem): Props

  def withActor(f: ActorRef => Unit)(implicit system: ActorSystem): Unit = {
    import system.dispatcher
    apply().onComplete {
      case Success(a) => f(a)
      case Failure(t) =>
        system.log.error(t, "withActor: unexpected failure")
        throw t
    }
  }

  def apply()(implicit system: ActorSystem): Future[ActorRef] = {
    val p = Promise[ActorRef]()
    system.actorOf(Props(new RetryingResolverCreator(p)))
    p.future
  }

}
