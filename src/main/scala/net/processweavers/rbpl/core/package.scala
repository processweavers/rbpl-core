package net.processweavers.rbpl

import akka.actor.Props

package object core {

  trait Repository

  trait Context

  case class Initializer[P](name: String, payload: P) {
    def description: String = payload.toString
  }

  //  implicit val typeMapperInitializerDefault = TypeMapper[InitializerPersisted, Initializer](f)(g)
  //  def f(sv: InitializerPersisted): Initializer = ???
  //  def g(uuid: Initializer): InitializerPersisted = InitializerPersisted()

  object Message {
    trait Reply
    trait Request[R <: Reply]
    trait Event
  }

}
