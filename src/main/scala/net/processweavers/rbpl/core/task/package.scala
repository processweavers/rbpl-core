package net.processweavers.rbpl.core

import java.time.{ Duration, Instant }
import java.util.UUID
import java.util.concurrent.TimeUnit

import akka.actor.{ Actor, ActorLogging, ActorRef, Props }
import akka.contrib.pattern.ReceivePipeline
import akka.contrib.pattern.ReceivePipeline.HandledCompletely
import akka.persistence.{ PersistentActor, RecoveryCompleted }
import net.processweavers.rbpl.core
import net.processweavers.rbpl.core.process.Process.CheckPredicate
import net.processweavers.rbpl.core.process.ProcessContext
import play.api.libs.json.{ Format, JsPath, Json }
import play.api.libs.functional.syntax._

import scala.concurrent.duration.FiniteDuration

package object task {

  type TaskName = String
  type TransitionName = String

  case class TaskContext(taskId: TaskId, mayBeInitiatingResult: Option[ResultId], mayBeTransistionName: Option[TransitionName]) extends Context

  case class FinderResult(maybeActorRef: Option[(TaskInfo, ActorRef)]) extends Message.Reply
  case class FinderResults(actorRefs: Seq[(TaskInfo, ActorRef)]) extends Message.Reply

  case class TaskInfo(name: TaskName)

  object Serializers {
    implicit val taskInfoFormat = Json.format[TaskInfo]
    implicit val uuidFormat: Format[UUID] =
      (JsPath \ "uuid").format[String].inmap(UUID.fromString, uuid => uuid.toString)
    implicit val taskIdFormat = Json.format[TaskId]
  }

  private[core] trait UntypedTaskActivator {
    def propsForUntyped(processContext: Context, taskContext: Context, i: Initializer[_])(): Props
  }

  trait TaskActivator[P] extends UntypedTaskActivator {
    def propsFor(processContext: ProcessContext, taskContext: TaskContext, initializer: Initializer[P])(): Props
    def propsForUntyped(processContext: Context, taskContext: Context, initializer: Initializer[_])(): Props =
      propsFor(processContext.asInstanceOf[ProcessContext], taskContext.asInstanceOf[TaskContext], initializer.asInstanceOf[Initializer[P]])
  }

  case class TaskDescriptor(name: TaskName, processContext: ProcessContext, context: TaskContext, initializerDescriptor: String)

  case class TaskId(id: UUID)
  object TaskId {
    def create = TaskId(UUID.randomUUID())
  }

  case class ResultId(id: UUID)
  object ResultId {
    def create = ResultId(UUID.randomUUID())
  }

  trait Task { _: Actor =>

    case object Initialize

    val descriptor: TaskDescriptor

    override def preStart(): Unit = self ! Initialize

    protected def parentProcess: ActorRef = context.parent

    def predicateArgument: Any = None

    def predicate(f: PartialFunction[Any, Boolean]): Boolean = if (f.isDefinedAt(predicateArgument)) f(predicateArgument) else false

    def receiveDefaultCheckPredicate: Receive = {
      case CheckPredicate(_) => sender ! None
    }
  }

  type TimerId = String

  class TimerTask(val descriptor: TaskDescriptor, duration: FiniteDuration, timerId: TimerId) extends Task with PersistentActor with ActorLogging {

    import TimerTask._

    val endTime = EndTime(Instant.now().plusMillis(duration.toMillis))

    var isRestarted = false

    def persistenceId: String = descriptor.context.taskId.id.toString

    def receiveRecover: Receive = {

      case et: EndTime if isExpired(et.tsp) =>
        isRestarted = true
        self ! timerExpiredMessage

      case et: EndTime =>
        isRestarted = true
        scheduleOnce(FiniteDuration(Duration.between(et.tsp, Instant.now).toMillis, TimeUnit.MILLISECONDS))

      case RecoveryCompleted if !isRestarted =>
        self ! endTime
    }

    def receiveCommand: Receive = receiveDefaultCheckPredicate orElse {

      case e: EndTime => persist(e) { et =>
        log.info("Setting endTime={} for timerId={}", e, timerId)
        scheduleOnce(duration)
      }

      case te: TimerExpired =>
        log.info("Timer expired for timerId={}", timerId)
        parentProcess ! te
    }

    private[core] def timerExpiredMessage = TimerExpired(timerId, descriptor.context, ResultId.create)

    private[core] def isExpired(i: Instant) = i.isBefore(Instant.now)

    private[core] def scheduleOnce(d: FiniteDuration) = {
      import context.dispatcher
      context.system.scheduler.scheduleOnce(d, self, timerExpiredMessage)
    }

    override def preStart(): Unit = {
      log.info("Starting timer with id={} and duration={}", timerId, duration)
    }
  }

  object TimerTask {
    case class TimerExpired(timerId: TimerId, taskContext: TaskContext, resultId: ResultId) extends TaskResult {
      override def description: String = s"TimerExpired(timerId=$timerId)"
    }

    case class EndTime(tsp: Instant)
    case class TimerTaskPayload(timerId: TimerId, duration: FiniteDuration)

    val activator = new TaskActivator[TimerTaskPayload] {
      def propsFor(processContext: ProcessContext, taskContext: TaskContext, i: Initializer[TimerTaskPayload])(): Props =
        Props(new TimerTask(TaskDescriptor(i.name, processContext, taskContext, i.description), i.payload.duration, i.payload.timerId))
    }

    def apply(timerId: TimerId, duration: FiniteDuration) =
      Initializer[TimerTaskPayload](taskNameForId(timerId), TimerTaskPayload(timerId, duration))

    val namePrefix = s"${classOf[TimerTask].getSimpleName}-"
    def taskNameForId(id: TimerId): TaskName = s"$namePrefix$id"
    def isTimerTask(taskName: TaskName): Boolean = taskName.startsWith(namePrefix)
    def timerIdFromName(taskName: TaskName): Option[TimerId] = {
      if (isTimerTask(taskName))
        Some(taskName.substring(namePrefix.length))
      else
        None
    }
  }

  trait TaskResult {
    val resultId: ResultId
    def taskContext: TaskContext
    def description: String = toString
  }

}
