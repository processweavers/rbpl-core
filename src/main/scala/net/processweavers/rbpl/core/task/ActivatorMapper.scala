package net.processweavers.rbpl.core.task

import net.processweavers.rbpl.core.Initializer
import net.processweavers.rbpl.core.process.ProcessPrivateApi.StartTask
import shapeless.Poly1

object ActivatorMapper extends Poly1 {
  implicit def caseSpecializedInitializer[P](implicit a: TaskActivator[P]) = at[Initializer[P]] { i =>
    StartTask(i, i.getClass.getName, a.getClass.getName, TaskContext(TaskId.create, None, None))
  }
}
