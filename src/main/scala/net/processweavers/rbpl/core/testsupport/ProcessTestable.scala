package net.processweavers.rbpl.core.testsupport

import akka.actor.{ ActorLogging, ActorRef, Props }
import akka.contrib.pattern.ReceivePipeline
import akka.persistence.PersistentActor
import net.processweavers.rbpl.core.process.{ CorrelationId, ProcessId, Process }
import net.processweavers.rbpl.core.task._

case class TaskStarted(taskDescriptor: TaskDescriptor)
case class TaskStopped(taskId: TaskId)
case class TimerStopped(timerId: TimerId)
case class ProcessStopped(pId: ProcessId, cId: CorrelationId)

trait ProcessTestable extends Process { _: PersistentActor with ReceivePipeline with ActorLogging =>

  val probe: ActorRef
  val genericTaskProbe: ActorRef

  override protected def stopTask(tid: TaskId): Unit = {
    val tn = activeTasks.find(_._1 == tid).map(_._2._1).getOrElse("")
    super.stopTask(tid)
    probe ! TimerTask.timerIdFromName(tn).fold[Any](TaskStopped(tid))(timerId => TimerStopped(timerId))
  }

  override protected def startTask(s: (TaskDescriptor, Props)): Unit = {
    super.startTask(s)
    probe ! TaskStarted(s._1)
  }

  override protected def newTaskActor(s: (TaskDescriptor, Props)): ActorRef = {
    genericTaskProbe
  }

  override protected def stopProcess(): Unit = {
    super.stopProcess()
    probe ! ProcessStopped(ctx.processId, ctx.correlationId)
  }
}
