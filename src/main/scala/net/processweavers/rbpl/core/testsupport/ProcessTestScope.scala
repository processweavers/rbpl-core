package net.processweavers.rbpl.core.testsupport

import java.util.concurrent.TimeUnit

import akka.actor.{ ActorRef, ActorSystem }
import akka.pattern.ask
import akka.testkit.TestProbe
import akka.util.Timeout
import net.processweavers.rbpl.core.process.{ CorrelationId, HistoryDotRenderer, ProcessContext, ProcessHistoryRecorder, ProcessId, ProcessName, Process }
import net.processweavers.rbpl.core.task.{ TaskDescriptor, TimerId, TimerTask }

import scala.concurrent.duration.Duration

abstract class ProcessTestScope(implicit val system: ActorSystem) {

  val waitForMessage = Duration(5, TimeUnit.SECONDS)

  def expectMsgAllOf[T](obj: T*): Seq[T] = {
    processProbe.expectMsgAllOf(obj: _*)
  }

  def expectTimerStarted(timerId: TimerId): TaskDescriptor = {
    val name = TimerTask.taskNameForId(timerId)
    processProbe.fishForMessage(waitForMessage, s"Waiting for Timer with id=$timerId to start") {
      case TaskStarted(TaskDescriptor(name, _, _, _)) => true
      case m =>
        system.log.warning("expectTimerStopped: Unexpected: {}", m)
        false
    }.asInstanceOf[TaskStarted].taskDescriptor
  }

  def expectTaskStarted(name: String): TaskDescriptor = {
    processProbe.fishForMessage(waitForMessage, s"Waiting for TaskStarted of $name") {
      case TaskStarted(TaskDescriptor(`name`, _, _, _)) => true
      case m =>
        system.log.warning("expectTaskStarted: Unexpected: {}", m)
        false
    }.asInstanceOf[TaskStarted].taskDescriptor
  }

  def expectTaskStopped(td: TaskDescriptor): Unit = {
    val taskStopped = processProbe.fishForMessage(waitForMessage, s"Waiting for TaskStopped of ${td.name} ") {
      case TaskStopped(td.context.taskId) => true
      case m =>
        system.log.warning("expectTaskStopped: Unexpected: {}", m)
        false
    }
  }

  def expectTimerStopped(id: TimerId): Unit = {
    val taskStopped = processProbe.fishForMessage(waitForMessage, s"Waiting for stopping of Timer $id ") {
      case TimerStopped(`id`) => true
      case m =>
        system.log.warning("expectTimerStopped: Unexpected: {}", m)
        false
    }
  }

  def expectProcessStopped(pId: ProcessId, cId: CorrelationId): Unit = {
    processProbe.fishForMessage(waitForMessage, s"Waiting for stopping of process (pId=$pId, cid=$cId") {
      case ProcessStopped(`pId`, `cId`) => true
      case m =>
        system.log.warning("expectProcessStopped: Unexpected: {}", m)
        false
    }
  }

  def dumpHistory(p: ActorRef, postfix: String): Unit = {
    import system.dispatcher
    implicit val t = Timeout(5, TimeUnit.SECONDS)
    (p ? Process.GetHistory).mapTo[ProcessHistoryRecorder.History].map { history =>
      println(s"\n\nRESULT LOG => file://${HistoryDotRenderer.toHtml(history, filenamePostFix = postfix)}\n\n")
    }
  }

  val processProbe = TestProbe()

  val processContext = ProcessContext(ProcessId.create, CorrelationId(""), ProcessName("Test"))

}
