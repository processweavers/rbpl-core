package net.processweavers.rbpl.core.testsupport

import net.processweavers.rbpl.core.process.{ CorrelationId, ProcessContext, ProcessId, ProcessName }
import net.processweavers.rbpl.core.task.{ TaskContext, TaskId }

abstract class TaskTestScope {
  val processContext = ProcessContext(ProcessId.create, CorrelationId(""), ProcessName("Test"))
  val taskContext = TaskContext(TaskId.create, None, None)
}
