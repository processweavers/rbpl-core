package net.processweavers.rbpl.core.process

import java.util.concurrent.TimeUnit

import akka.actor.{ ActorLogging, ActorRef, Props }
import akka.contrib.pattern.ReceivePipeline
import akka.contrib.pattern.ReceivePipeline.{ HandledCompletely, Inner }
import akka.pattern.ask
import akka.persistence.{ PersistentActor, PersistentRepr, RecoveryCompleted }
import akka.util.Timeout
import net.processweavers.rbpl.core._
import net.processweavers.rbpl.core.process.ProcessPrivateApi._
import net.processweavers.rbpl.core.process.ProcessRepo.FindWithPredicate
import net.processweavers.rbpl.core.task._
import shapeless.HList
import shapeless.ops.hlist.{ Mapper, ToList }

import scala.concurrent.Future
import scala.concurrent.duration.FiniteDuration
import scala.util.control.ControlThrowable
import scala.util.{ Failure, Success }

protected[core] trait Process {
  _: PersistentActor with ReceivePipeline with ActorLogging =>

  import AndThenAPI._

  var historyRecorder: ProcessHistoryRecorder

  protected def persistAndHandleResult[A <: TaskResult](result: A)(action: A => Unit): AndThenAPI =
    (andThenActions: Seq[AndThenAction]) => persist(result) { e1 =>
      try {
        log.debug("result persisted {}", e1)
        recordTaskResult(e1)
        action(e1)
        val msgsToProcess = andThenActions.map(_ match {
          case t: AndThenTask => t.execute(e1, self)
          case spa: StopProcessAction => spa.execute(ctx.correlationId, self)
        })
        self ! ProcessAll(msgsToProcess)
      } catch {
        case c: ControlThrowable => throw c // propagate
        case t: Throwable =>
          throw new IllegalStateException("Error caught while invoking persistAndHandleResult handler", t)
      }
    }

  protected def persistResult[A <: TaskResult](result: A): AndThenAPI =
    persistAndHandleResult(result) { _ => }

  protected val ctx: ProcessContext

  def predicateArgument: Any

  def checkPredicate(f: PartialFunction[Any, Boolean]): Boolean = {
    val arg = predicateArgument
    if (f.isDefinedAt(arg)) f(arg) else false
  }

  def persistenceId: String = s"${getClass.getSimpleName}-${ctx.processId}"

  var activeTasks: Map[TaskId, (TaskName, ActorRef)] = Map.empty
  private var completedTasks: Map[TaskId, (TaskName, ActorRef)] = Map.empty

  protected def startTask(s: Tuple2[TaskDescriptor, Props]): Unit = {
    val a = newTaskActor(s)
    activeTasks = activeTasks + (s._1.context.taskId -> (s._1.name, a))
    recordTaskStarted(s)
    log.info(s"New task started: ${s._1}, ref: $a")
  }

  protected def newTaskActor(s: (TaskDescriptor, Props)): ActorRef =
    context.actorOf(s._2, s"${s._1.name}-${s._1.context.taskId.id}")

  protected def stopTask(tid: TaskId): Unit = {
    if (activeTasks.contains(tid)) {
      val task = activeTasks.apply(tid)
      context.stop(task._2)
      completedTasks = completedTasks + (tid -> task)
      activeTasks = activeTasks - tid
      recordTaskStopped(tid)
      log.info("Task stopped: {}", tid)
    } else {
      log.warning("stopTask failed: Task with taskid={} not found!", tid)
    }
  }

  protected def recordTaskStarted(s: (TaskDescriptor, Props)): Unit =
    historyRecorder = historyRecorder.recordTaskStarted(s._1)

  protected def recordTaskResult[A <: TaskResult](e1: A): Unit =
    historyRecorder = historyRecorder.recordTaskResult(e1)

  protected def recordTaskStopped(tid: TaskId): Unit =
    historyRecorder = historyRecorder.recordTaskStopped(tid)

  protected def allActiveTasks: Map[TaskId, (TaskName, ActorRef)] = activeTasks

  protected def allCompletedTasks: Map[TaskId, (TaskName, ActorRef)] = completedTasks

  protected def stopProcess(): Unit = {
    log.info("stop process, forward to repo")
    context.parent ! ProcessRepo.ProcessFinished(ctx.correlationId)
  }
}

object Process {

  case object GetHistory

  case class CheckPredicate(p: PartialFunction[Any, Boolean])

  case object AllActiveTasks extends Message.Request[ActiveTasks]
  case class ActiveTasks(ts: Map[TaskId, (TaskName, ActorRef)]) extends Message.Reply

}

private[core] object ProcessPrivateApi {

  sealed trait ProcessApi

  case class StopTask(taskId: TaskId, resultDescription: String) extends ProcessApi

  case class StopTaskByName(taskName: TaskName, resultDescription: String) extends ProcessApi

  case class StartTask[P](initializer: Initializer[P], initializerClassName: String, activatorClassName: String, taskContext: TaskContext) extends ProcessApi

  case class StartTimer[P, A <: TaskActivator[P]](initializer: Initializer[P], activator: A, taskId: TaskId, timerId: String) extends ProcessApi

  case class StopTimer(timerId: TimerId, description: String) extends ProcessApi

  case object StopProcess extends ProcessApi

  case class ProcessAll(privateApiMessages: Seq[ProcessApi])

}

abstract class BaseProcess[T <: HList, RES <: HList](val initialTasks: T, val processInitializer: Option[Initializer[_]] = None)(implicit activatorMapper: Mapper.Aux[ActivatorMapper.type, T, RES], _toList: ToList[RES, StartTask[_]])
  extends ReceivePipeline
  with PersistentActor
  with Process
  with ActorLogging {

  import Process._

  implicit val timeout = Timeout(FiniteDuration(1, TimeUnit.SECONDS))

  var historyRecorder = new ProcessHistoryRecorder(processInitializer.map(_.name).getOrElse(getClass.getSimpleName), processInitializer.map(_.payload))

  // required for recovery only
  private[core] var tasksStarted: Map[TaskId, (TaskDescriptor, Props)] = Map.empty
  private[core] var tasksStopped: List[TaskId] = List.empty

  pipelineOuter {

    case pa: ProcessPrivateApi.ProcessAll if pa.privateApiMessages.isEmpty =>
      HandledCompletely

    case pa: ProcessPrivateApi.ProcessAll if pa.privateApiMessages.nonEmpty =>
      persistAll(scala.collection.immutable.Seq(pa.privateApiMessages: _*)) {

        case e: ProcessPrivateApi.StartTask[_] =>
          val activator = Class.forName(e.activatorClassName).newInstance().asInstanceOf[UntypedTaskActivator]
          startTask(
            TaskDescriptor(e.initializer.name, ctx, e.taskContext, e.initializer.description),
            activator.propsForUntyped(ctx, e.taskContext, e.initializer))

        case ProcessPrivateApi.StopTask(tId, _) =>
          stopTask(tId)

        case stbn: ProcessPrivateApi.StopTaskByName =>
          activeTasks.find(t => t._2._1 == stbn.taskName).foreach(t => stopTask(t._1))

        case ProcessPrivateApi.StopTimer(timerId, description) =>
          activeTasks.find(t => t._2._1 == TimerTask.taskNameForId(timerId)).foreach(t => stopTask(t._1))

        case StopProcess =>
          stopProcess()

      }
      HandledCompletely

    case GetHistory =>
      sender ! historyRecorder.history
      HandledCompletely

    case CheckPredicate(p) =>
      sender ! (if (checkPredicate(p)) Some((ctx, self)) else None)
      HandledCompletely

    case PersistentRepr(st: StartTask[_], _) =>
      val activator = Class.forName(st.activatorClassName).newInstance().asInstanceOf[UntypedTaskActivator]
      val initializer = st.initializer
      tasksStarted = tasksStarted + (st.taskContext.taskId -> (TaskDescriptor(initializer.name, ctx, st.taskContext, initializer.description), activator.propsForUntyped(ctx, st.taskContext, initializer)))
      HandledCompletely

    case PersistentRepr(st: StopTask, _) =>
      tasksStopped = st.taskId :: tasksStopped
      HandledCompletely

    case pr @ PersistentRepr(res: TaskResult, _) =>
      Inner(pr).andAfter {
        recordTaskResult(res)
      }

    case RecoveryCompleted =>
      Inner(RecoveryCompleted).andAfter {
        // Restart all non-completed tasks
        tasksStarted.filterNot(started => tasksStopped.contains(started._1)).values.foreach(startTask)
        // Restart potentially not started initial tasks
        self ! ProcessAll(InitialTasksToStart.findNotStarted(activatorMapper(initialTasks).toList, tasksStarted))
        // Record start only for completed tasks
        tasksStarted.filter(started => tasksStopped.contains(started._1)).values.foreach(recordTaskStarted)
        // Record stop for completed tasks
        tasksStopped.foreach(recordTaskStopped)
        log.debug("Recovery completed for persistenceId={}", persistenceId)
      }

    case FindWithPredicate(p) =>
      val _sender = sender
      val at = activeTasks.values.map(a => (a._2 ? CheckPredicate(p)).mapTo[Option[(TaskInfo, ActorRef)]])
      import context.dispatcher
      Future.sequence(at)
        .onComplete {
          case Success(bs) =>
            log.debug("Found tasks: {}", bs)
            _sender ! task.FinderResults(bs.flatten.toSeq)
          case Failure(t) =>
            log.error(t, "Failure while evaluating predicate {}", p)
            _sender ! task.FinderResults(Nil)
        }
      HandledCompletely

    case AllActiveTasks =>
      sender ! ActiveTasks(activeTasks)
      HandledCompletely
  }

  override protected def onPersistRejected(cause: Throwable, event: Any, seqNr: Long): Unit = {
    super.onPersistRejected(cause, event, seqNr)
    throw new IllegalStateException("onPersistRejected", cause)
  }
}

object InitialTasksToStart {
  def findNotStarted(initialTasks: Seq[StartTask[_]], tasksStarted: Map[TaskId, (TaskDescriptor, Props)]): Seq[StartTask[_]] =
    initialTasks.takeRight(Math.max(initialTasks.size - tasksStarted.size, 0))
}
