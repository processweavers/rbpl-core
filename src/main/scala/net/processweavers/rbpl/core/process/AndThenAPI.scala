package net.processweavers.rbpl.core.process

import akka.actor.ActorRef
import net.processweavers.rbpl.core.Initializer
import net.processweavers.rbpl.core.process.ProcessPrivateApi.{ ProcessApi, StartTask }
import net.processweavers.rbpl.core.task._

import scala.concurrent.duration.FiniteDuration

object AndThenAPI {

  sealed trait AndThenAction

  trait AndThenTask extends AndThenAction {
    def execute(result: TaskResult, po: ActorRef): ProcessApi
  }

  trait AndThenAPI {
    def andThen(andThenActions: AndThenAction*)
  }

  object StopCurrentTask {
    def apply(): AndThenTask =
      (result: TaskResult, p: ActorRef) => ProcessPrivateApi.StopTask(result.taskContext.taskId, result.description)
  }

  object StopTask {
    def apply(taskName: String): AndThenTask =
      (result: TaskResult, p: ActorRef) => ProcessPrivateApi.StopTaskByName(taskName, result.description)
  }

  object StartNewTask {
    def apply[P, A <: TaskActivator[P]](transitionName: TransitionName, i: Initializer[P])(implicit a: A): AndThenTask = (result: TaskResult, p: ActorRef) => {
      val taskId = TaskId.create
      StartTask(i, i.getClass.getName, a.getClass.getName, TaskContext(taskId, Some(result.resultId), Some(transitionName)))
    }
  }

  object StartTimer {
    def apply(timerId: TimerId, duration: FiniteDuration): AndThenTask = (result: TaskResult, p: ActorRef) => {
      val act = TimerTask.activator
      val i = TimerTask(timerId, duration)
      StartTask(i, i.getClass.getName, act.getClass.getName, TaskContext(TaskId.create, Some(result.resultId), Some("start timer")))
    }
  }

  object StopCurrentTimer {
    def apply(): AndThenTask = (result: TaskResult, p: ActorRef) => ProcessPrivateApi.StopTask(result.taskContext.taskId, result.description)
  }

  object StopTimer {
    def apply(timerId: TimerId): AndThenTask = (result: TaskResult, p: ActorRef) => ProcessPrivateApi.StopTimer(timerId, result.description)
  }

  trait StopProcessAction extends AndThenAction {
    def execute(corrId: CorrelationId, po: ActorRef): ProcessApi
  }

  object StopProcess {
    def apply(): StopProcessAction = (corrId: CorrelationId, p: ActorRef) => ProcessPrivateApi.StopProcess
  }

}
