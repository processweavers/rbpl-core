package net.processweavers.rbpl.core.process

import java.io.{ File, FileWriter }
import java.time.Instant
import java.util.UUID

import net.processweavers.rbpl.util._

import net.processweavers.rbpl.core.task.{ ResultId, TaskId }

object HistoryDotRenderer {

  implicit def taskIdToUUID(tid: TaskId): UUID = tid.id

  val TempDirName = System.getProperty("java.io.tmpdir")

  trait Node {
    val id: UUID
    val label: String
    val description: String
  }

  case class SplitGateway(gwId: UUID, label: String, description: String = "", id: UUID = UUID.randomUUID()) extends Node
  case class ParallelMergeGateway(gwId: UUID, label: String, incomingTasks: Set[TaskId], description: String = "", id: UUID = UUID.randomUUID()) extends Node
  case class Result(id: UUID, label: String, description: String) extends Node
  case class Task(id: UUID, label: String, description: String, isActive: Boolean = false) extends Node

  case object StartNode extends Node {
    val id: UUID = UUID.randomUUID()
    val description: String = ""
    val label: String = "Start"
  }

  case object EndNode extends Node {
    val id: UUID = UUID.randomUUID()
    val description: String = ""
    val label: String = "End"
  }

  case class Transition(from: UUID, to: UUID, label: Option[String] = None)

  case class DotModel(label: String, processPayload: String, lastUpdated: Instant, results: Map[ResultId, Result] = Map.empty, tasks: Map[TaskId, Task] = Map.empty, transitions: List[Transition] = Nil)

  def toDot(history: ProcessHistoryRecorder.History): String =
    toDotSpec(toDotModel(history))

  def toHtml(history: ProcessHistoryRecorder.History, targetDir: File = new File(TempDirName), filenamePostFix: String = ""): String = {

    import java.nio.file.Files.copy

    val expectedVizJsFile = new File(targetDir, "viz.js")
    if (!expectedVizJsFile.exists()) {
      Option(getClass.getClassLoader.getResourceAsStream("assets/viz.js/viz.js")).map { vizJs =>
        copy(vizJs, expectedVizJsFile.toPath)
      }
    }

    val html =
      s"""
        |<!DOCTYPE html>
        |<html>
        |  <head>
        |    <meta charset="utf-8">
        |    <title>Process Diagram ${history.label}</title>
        |  </head>
        |  <body>
        |    <script src="./viz.js"></script>
        |    <script>
        |    var dot = `
        |    ${toDotSpec(toDotModel(history))}
        |    `;
        |    document.body.innerHTML += Viz(dot);
        |    </script>
        |
        |  </body>
        |</html>
      """.stripMargin

    val htmlFile = new File(targetDir, s"${history.label + filenamePostFix}.html")
    using(new FileWriter(htmlFile)) { f =>
      f.write(html)
    }
    htmlFile.getAbsolutePath
  }

  def toDotModel(history: ProcessHistoryRecorder.History): DotModel = {

    val ts = history.transitions.map(t => Transition(t.from.getOrElse(ResultId(StartNode.id)).id, t.to.getOrElse(TaskId(EndNode.id)).id, t.name)) ++ history.results.map(r => Transition(r._1, r._2.resultId.id))
    // Add transitions for "final" results, i.e. results that initiate no new tasks (have no transition created above)
    val finalTs = history.results.filterNot(r => ts.exists(_.from == r._2.resultId.id)).map(r => Transition(r._2.resultId.id, EndNode.id))

    DotModel(
      history.label,
      history.processPayload,
      history.lastUpdated,
      history.results.map(r => r._2.resultId -> Result(r._2.resultId.id, "", r._2.description)),
      history.tasks.map(t => t._1 -> Task(t._1, t._2.name, t._2.initializerDescription, !t._2.isStopped)),
      ts ++ finalTs)
  }

  def toDotSpec(model: DotModel): String = {

    def toDotId(id: String) = s"_${id.replace("-", "_")}"

    case class DotId(id: String) {
      override def toString: String = id
    }

    implicit def uuidToDotId(uuid: UUID): DotId = DotId(s"_${uuid.toString.replace("-", "_")}")

    def transition(from: DotId, to: DotId, label: String): String =
      s"""
         | $from -> $to [label="$label"]
      """.stripMargin

    def task(tid: TaskId, s: String, initializerDescription: String, isActive: Boolean): String =
      s"""
         | ${toDotId(tid.id.toString)} [style="rounded,bold" color=${if (isActive) "red" else "black"} shape="box" label=<
         |   <TABLE BORDER="0" CELLBORDER="0" CELLSPACING="0" CELLPADDING="4" COLOR="BLACK">
         |   <TR><TD>$s</TD></TR>
         |   <TR><TD><FONT POINT-SIZE="8">${tid.id.toString}</FONT></TD></TR>
         |   <TR><TD><FONT POINT-SIZE="8">${wrapStringHtml(initializerDescription)}</FONT></TD></TR>
         |   </TABLE>
         | >]
      """.stripMargin

    def result(rid: ResultId, r: Result): String =
      s"""
         | ${toDotId(rid.id.toString)} [style="dashed,roundes" shape="box" label=<
         |   <TABLE BORDER="0" CELLBORDER="0" CELLSPACING="0" CELLPADDING="4" COLOR="BLACK">
         |   <TR><TD><FONT POINT-SIZE="8">${wrapStringHtml(r.description)}</FONT></TD></TR>
         |   </TABLE>
         | >]
      """.stripMargin

    def tasks: String = model.tasks.filterNot(t => List(StartNode.id, EndNode.id).contains(t._1.id)).map(t => task(t._1, t._2.label, t._2.description, t._2.isActive)).mkString("\n")

    def results: String = model.results.map(r => result(r._1, r._2)).mkString("\n")

    def transitions: String = model.transitions.map(t => transition(t.from, t.to, t.label.getOrElse(""))).mkString("\n")

    def wrapString(s: String, sep: String): String = s.splitAt(50) match {
      case (x, "") => x
      case (x, y) => x + sep + wrapString(y, sep)
    }

    def endNodeIfTransitionsToIt(endNode: Node, ts: List[Transition]): String = {
      if (ts.exists(_.to == endNode.id))
        s"""${toDotId(endNode.id.toString)} [label="${endNode.label}" shape=doublecircle];"""
      else
        "# no transitions pointing to the end node -> omitting it"
    }

    def wrapStringHtml(s: String): String = wrapString(s, "<BR/>")
    def wrapStringText(s: String): String = wrapString(s, "\n")

    s"""
       |digraph G {
       | subgraph cluster_0 {
       |  label=<
       |   <TABLE BORDER="0" CELLBORDER="0" CELLSPACING="0" CELLPADDING="4">
       |   <TR><TD><B>${model.label}</B></TD></TR>
       |   <TR><TD><FONT POINT-SIZE="8">${model.lastUpdated}</FONT></TD></TR>
       |   </TABLE>
       | >;
       |  shape=ellipse style="bold, rounded";
       |  # Start
       |	${toDotId(StartNode.id.toString)} [shape=circle style="bold" margin=0 fontsize=8 label="${wrapStringText(model.processPayload)}"];
       |  # End
       |  ${endNodeIfTransitionsToIt(EndNode, model.transitions)}
       |  # Results
       |  $results
       |  # Tasks
       |  $tasks
       |  # Transtions
       |  $transitions
       | }
       |}
    """.stripMargin
  }
}