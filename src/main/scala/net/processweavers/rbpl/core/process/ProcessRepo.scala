package net.processweavers.rbpl.core.process

import java.util.concurrent.TimeUnit

import akka.actor.{ ActorLogging, ActorRef, ActorSystem, Props }
import akka.pattern.ask
import akka.persistence.{ PersistentActor, RecoveryCompleted }
import akka.util.Timeout
import net.processweavers.rbpl.core.process.Process.CheckPredicate
import net.processweavers.rbpl.core.{ Initializer, Message, Repository }
import net.processweavers.rbpl.util.SingletonActorSupport

import scala.concurrent.duration.FiniteDuration
import scala.concurrent.{ Await, Future }
import scala.util.{ Failure, Success }

object ProcessRepo extends SingletonActorSupport {

  val ACTORNAME = "ProcessRepo"

  protected def newActorProps(implicit system: ActorSystem) = Props[ProcessRepo]

  implicit val timeout = Timeout(FiniteDuration(1, TimeUnit.SECONDS))

  type Predicate = PartialFunction[Any, Boolean]

  val FindAllPredicate: Predicate = { case _ => true }

  case class StartNewProcess[P, A <: ProcessActivator[P]](cId: CorrelationId, initializer: Initializer[P])(implicit val activator: A) extends Message.Request[NewProcessStarted]
  case class NewProcessStarted(ctx: ProcessContext) extends Message.Reply
  case class ProcessFinished(cId: CorrelationId) extends Message.Event
  case class FindWithCorrelationId(cId: CorrelationId) extends Message.Request[FinderResult]
  case class FindWithPredicate(p: Predicate) extends Message.Request[FinderResults]
  case class FinderResult(maybeProcess: Option[(ProcessContext, ActorRef)]) extends Message.Reply
  case class FinderResults(maybeProcesses: Seq[(ProcessContext, ActorRef)]) extends Message.Reply

}

private[core] class ProcessRepo extends PersistentActor with ActorLogging with Repository {

  import ProcessRepo._
  import ProcessRepoPrivateApi._
  implicit val system = context.system

  def persistenceId: String = self.path.toStringWithoutAddress

  var activeProcessesByCorrelationId: Map[CorrelationId, (ProcessContext, ActorRef)] = Map.empty

  // only for recovery
  var processesToRestart: Map[CorrelationId, ProcessStarted[_]] = Map.empty

  def receiveRecover: Receive = {

    case ps: ProcessStarted[_] =>
      processesToRestart = processesToRestart + (ps.pContext.correlationId -> ps)

    case ProcessFinished(cId) =>
      processesToRestart = processesToRestart - cId

    case RecoveryCompleted =>
      processesToRestart.values foreach startProcess
  }

  def startProcess(ps: ProcessStarted[_]): Unit = {
    val activator = Class.forName(ps.activatorClassName).newInstance().asInstanceOf[UntypedProcessActivator]
    val props = activator.propsForUntyped(ps.pContext, ps.initializer)
    val a = context.actorOf(props, s"${ps.initializer.name}-${ps.pContext.processId.id}")
    activeProcessesByCorrelationId = activeProcessesByCorrelationId + (ps.pContext.correlationId -> (ps.pContext, a))
    log.info("Process started with id={}, ref={}", ps.pContext.processId, a)
  }

  def receiveCommand: Receive = {

    case s: StartNewProcess[_, _] =>
      persist(ProcessStarted(s.initializer, s.initializer.getClass.getName, s.activator.getClass.getName, ProcessContext(ProcessId.create, s.cId, ProcessName(s.initializer.name), s.initializer.description))) { ps =>
        startProcess(ps)
        sender ! NewProcessStarted(ps.pContext)
      }

    case pf: ProcessFinished =>
      activeProcessesByCorrelationId.get(pf.cId).fold(
        log.warning("CorrelationId {} not known!", pf.cId)) { a =>
          persist(pf) { p =>
            log.info("Stopping actor {}", a)
            context.stop(a._2)
            activeProcessesByCorrelationId = activeProcessesByCorrelationId - p.cId
          }
        }

    case FindWithCorrelationId(cId) =>
      sender ! FinderResult(activeProcessesByCorrelationId.get(cId))

    case FindWithPredicate(p) =>
      val _sender = sender
      val fs: Iterable[Future[Option[(ProcessContext, ActorRef)]]] = activeProcessesByCorrelationId.values.map(a => (a._2 ? CheckPredicate(p)).mapTo[Option[(ProcessContext, ActorRef)]])
      import context.dispatcher
      Future.sequence(fs).onComplete {
        case Success(bs) =>
          _sender ! FinderResults(bs.flatten.toSeq)
        case Failure(t) =>
          log.error(t, "Failure while evaluating predicate {}", p)
          _sender ! FinderResults(Nil)
      }
  }

}

private[process] object ProcessRepoPrivateApi {
  case class ProcessStarted[P](initializer: Initializer[P], initializerClassName: String, activatorClassName: String, pContext: ProcessContext)

}

