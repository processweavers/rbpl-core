package net.processweavers.rbpl.core.process.serialization

import akka.actor.ExtendedActorSystem
import akka.serialization.{ SerializationExtension, SerializerWithStringManifest }

import scala.reflect.ClassTag

abstract class BaseProcessSerializer(val system: ExtendedActorSystem) extends SerializerWithStringManifest
  with SerializationSupport {
  lazy val serializerExt = SerializationExtension(system)

  class PersistanceMapper[T <: AnyRef, U <: com.trueaccord.scalapb.GeneratedMessage](f: Array[Byte] => U)(implicit tu: Function1[T, U], ut: Function1[U, T], val ct: ClassTag[T]) {
    def toPersRep(a: AnyRef): Array[Byte] = tu(a.asInstanceOf[T]).toByteArray
    def fromBytes(bs: Array[Byte]): AnyRef = ut(f(bs))
  }

  // required for mapping of the manifest (class name) to the appropriate mapper (when reading from persistence)
  val ms: Map[String, PersistanceMapper[_, _]]

  // optimization for mapping a model class to the appropriate mapper (when writing to persistence)
  lazy val msInverted: Map[Class[_], String] = ms.map(t => t._2.ct.runtimeClass -> t._1).toMap

  // Use the class name of the model class as manifest, potentially add a version if needed
  override def manifest(o: AnyRef): String = o.getClass.getName

  // Map the persistent representation (protobuf model) back into an application model
  override def fromBinary(bytes: Array[Byte], manifest: String): AnyRef =
    ms.get(manifest).fold(throw new RuntimeException(s"No PersistanceMapper found for manifest=$manifest"))(_.fromBytes(bytes))

  // Map the application model to its persistent representation
  override def toBinary(o: AnyRef): Array[Byte] =
    msInverted.get(o.getClass).fold(throw new RuntimeException(s"No PersistanceMapper found for instance=$o"))(m => ms(m).toPersRep(o))

}
