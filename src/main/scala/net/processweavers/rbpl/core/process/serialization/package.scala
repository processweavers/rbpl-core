package net.processweavers.rbpl.core.process

import java.time.Instant
import java.util.UUID
import java.util.concurrent.TimeUnit

import net.processweavers.rbpl.core.process.ProcessPrivateApi.StopTask
import net.processweavers.rbpl.core.process.persmodels._
import net.processweavers.rbpl.core.task.TimerTask.TimerTaskPayload
import net.processweavers.rbpl.core.task.{ ResultId, TaskContext, TaskId, TimerTask }

import scala.concurrent.duration.FiniteDuration

package object serialization {

  implicit def resultIdtoString(t: ResultId): String = t.id.toString
  implicit def stringToResultId(u: String): ResultId = ResultId(UUID.fromString(u))

  implicit def taskIdtoString(t: TaskId): String = t.id.toString
  implicit def stringToTaskId(u: String): TaskId = TaskId(UUID.fromString(u))

  implicit def processIdtoString(t: ProcessId): String = t.id.toString
  implicit def stringToProcessId(u: String): ProcessId = ProcessId(UUID.fromString(u))

  implicit def fromTaskContext(tc: TaskContext): TaskContextPersisted = TaskContextPersisted(
    tc.taskId.id.toString,
    tc.mayBeInitiatingResult.map(x => x),
    tc.mayBeTransistionName)
  implicit def fromTaskContextPersisted(tcP: TaskContextPersisted): TaskContext = TaskContext(
    tcP.taskId,
    tcP.resultId.map(uuid => uuid),
    tcP.transitionName)

  implicit def fromStopTask(t: StopTask): StopTaskReceivedPersisted =
    StopTaskReceivedPersisted(t.taskId)
  implicit def fromStopTaskReceivedPersisted(u: StopTaskReceivedPersisted): StopTask = {
    ProcessPrivateApi.StopTask(u.id, u.resultDescription)
  }

  implicit def fromTimerTaskPayload(t: TimerTaskPayload): TimerTaskPayloadPersisted =
    TimerTaskPayloadPersisted(t.timerId, t.duration.toMillis)
  implicit def fromTimerTaskPayloadPersisted(u: TimerTaskPayloadPersisted): TimerTaskPayload =
    TimerTaskPayload(u.timerId, FiniteDuration(u.duration, TimeUnit.MILLISECONDS))

  implicit def fromTimerTaskEndTimePersisted(t: TimerTaskEndTimePersisted): TimerTask.EndTime =
    TimerTask.EndTime(Instant.ofEpochMilli(t.tsp))
  implicit def fromTimerTaskEndTime(t: TimerTask.EndTime): TimerTaskEndTimePersisted =
    TimerTaskEndTimePersisted(t.tsp.toEpochMilli)

  implicit def fromTimerTaskTimerExpiredPersisted(t: TimerTaskTimerExpiredPersisted): TimerTask.TimerExpired =
    TimerTask.TimerExpired(
      t.timerId,
      t.taskContext.get,
      t.resultId)
  implicit def fromTimerTaskTimerExpired(t: TimerTask.TimerExpired): TimerTaskTimerExpiredPersisted =
    TimerTaskTimerExpiredPersisted(
      t.timerId,
      t.resultId,
      Some(t.taskContext))

  implicit def fromProcessContextPersisted(str: ProcessContextPersisted): ProcessContext =
    ProcessContext(
      str.processId,
      CorrelationId(str.correlationId),
      ProcessName(str.processName),
      str.description)
  implicit def fromProcessContext(a: ProcessContext): ProcessContextPersisted =
    ProcessContextPersisted(
      a.processId,
      a.correlationId.s,
      a.name.n,
      a.description)

  implicit def fromProcessRepoProcessFinished(a: ProcessRepo.ProcessFinished): ProcessRepoProcessFinishedPersisted =
    ProcessRepoProcessFinishedPersisted(a.cId.s)
  implicit def fromProcessRepoProcessFinishedPersisted(a: ProcessRepoProcessFinishedPersisted): ProcessRepo.ProcessFinished =
    ProcessRepo.ProcessFinished(CorrelationId(a.correlationId))

}
