package net.processweavers.rbpl.core.process.serialization

import akka.serialization.{ Serialization, Serializer, SerializerWithStringManifest }
import com.google.protobuf.ByteString
import com.google.protobuf.any.{ Any => ProtobufAny }
import net.processweavers.rbpl.core.process.persmodels.SerializedWithManifest

import scala.reflect.ClassTag
import scala.util.Try

class NoSerializerAvailableException(subject: scala.Any) extends RuntimeException(s"!!! No serializer found for class: ${subject.getClass.getName} / instance: $subject !!!")

class NoManifestBasedSerializerAvailableException(subject: scala.Any) extends RuntimeException(s"!!! No manifest-based serializer found for class: ${subject.getClass.getName} / instance: $subject !!!")

trait SerializationSupport {

  val serializerExt: Serialization

  def tryGetSerializer(x: AnyRef): Serializer =
    Try { serializerExt.findSerializerFor(x) }.getOrElse(throw new NoSerializerAvailableException(x))

  def tryGetSerializer(ct: ClassTag[_]): Serializer =
    Try { serializerExt.serializerFor(ct.runtimeClass) }.getOrElse(throw new NoSerializerAvailableException(ct.runtimeClass))

  // The wrapper is required for recursive use of our serializer ... which requires a manifest normally provided by akka persistence
  def trySerialize(x: AnyRef): com.google.protobuf.ByteString = {
    val wrapped = tryGetSerializer(x) match {
      case s: SerializerWithStringManifest =>
        SerializedWithManifest(s.manifest(x), ByteString.copyFrom(s.toBinary(x)))
      case s =>
        SerializedWithManifest("", ByteString.copyFrom(s.toBinary(x)))
    }
    ByteString.copyFrom(wrapped.toByteArray)
  }

  def tryDeserializeAs[T](raw: ByteString)(implicit ct: ClassTag[T]): T = {
    val manifestWrapper = SerializedWithManifest.parseFrom(raw.toByteArray)
    val unwrapped = tryGetSerializer(ct) match {
      case s: SerializerWithStringManifest =>
        s.fromBinary(manifestWrapper.payload.toByteArray, manifestWrapper.manifest)
      case s =>
        s.fromBinary(manifestWrapper.payload.toByteArray)
    }
    unwrapped.asInstanceOf[T]
  }

  def trySerializeToProtobufAny(x: Any): ProtobufAny = {
    val anyRef = x.asInstanceOf[AnyRef] // must be an AnyRef!
    val s = Try { tryGetSerializer(anyRef).asInstanceOf[SerializerWithStringManifest] }.getOrElse(throw new NoManifestBasedSerializerAvailableException(anyRef))
    ProtobufAny(
      s"type.googleapis.com/${s.manifest(anyRef)}",
      com.google.protobuf.ByteString.copyFrom(s.toBinary(anyRef)))
  }

  def tryDeserializeFromProtobufAny(pbAny: ProtobufAny): AnyRef = {
    val typeName = pbAny.typeUrl.split("/")(1)
    val clazz = serializerExt.system.dynamicAccess.getClassFor[scala.Any](typeName).get
    val serializer = serializerExt.serializerFor(clazz).asInstanceOf[SerializerWithStringManifest]
    serializer.fromBinary(pbAny.value.toByteArray, clazz)
  }

}
