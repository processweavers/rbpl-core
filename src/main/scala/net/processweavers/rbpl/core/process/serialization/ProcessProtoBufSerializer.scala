package net.processweavers.rbpl.core.process.serialization

import akka.actor.ExtendedActorSystem
import net.processweavers.rbpl.core.Initializer
import net.processweavers.rbpl.core.process.ProcessPrivateApi.{ StopTask, StartTask }
import net.processweavers.rbpl.core.process._
import net.processweavers.rbpl.core.process.persmodels._
import net.processweavers.rbpl.core.task.TimerTask.TimerTaskPayload
import net.processweavers.rbpl.core.task.{ TaskContext, TimerTask }

class ProcessProtoBufSerializer(system: ExtendedActorSystem) extends BaseProcessSerializer(system) {

  override val identifier: Int = 9001

  // requires serializer extension and can thus not reside in the package object
  implicit def fromTaskStarted(a: StartTask[_]): TaskStartedPersisted = TaskStartedPersisted(
    Some(trySerializeToProtobufAny(a.initializer.payload)),
    a.initializer.name,
    a.initializerClassName,
    a.activatorClassName,
    Some(a.taskContext))

  implicit def fromTaskStartedPersisted(u: TaskStartedPersisted): StartTask[_] = StartTask(
    Initializer(u.name, tryDeserializeFromProtobufAny(u.initializer.get)),
    u.initializerClassName,
    u.activatorClassName,
    u.taskContext.get)
  implicit def fromProcessRepoPrivateApiProcessStarted(a: ProcessRepoPrivateApi.ProcessStarted[_]): ProcessRepoPrivateApiProcessStartedPersisted =
    ProcessRepoPrivateApiProcessStartedPersisted(
      Some(trySerializeToProtobufAny(a.initializer.payload)),
      a.initializer.name,
      a.initializerClassName,
      a.activatorClassName,
      Some(a.pContext))
  implicit def fromProcessRepoPrivateApiProcessStartedPersisted(str: ProcessRepoPrivateApiProcessStartedPersisted): ProcessRepoPrivateApi.ProcessStarted[_] =
    ProcessRepoPrivateApi.ProcessStarted(
      Initializer(str.name, tryDeserializeFromProtobufAny(str.initializer.get)),
      str.initializerClassName,
      str.activatorClassName,
      str.processContext.get)

  val ms: Map[String, PersistanceMapper[_, _]] = Map(
    classOf[StopTask].getName ->
      new PersistanceMapper[StopTask, StopTaskReceivedPersisted](StopTaskReceivedPersisted.parseFrom),
    classOf[TimerTaskPayload].getName ->
      new PersistanceMapper[TimerTaskPayload, TimerTaskPayloadPersisted](TimerTaskPayloadPersisted.parseFrom),
    classOf[TaskContext].getName ->
      new PersistanceMapper[TaskContext, TaskContextPersisted](TaskContextPersisted.parseFrom),
    classOf[StartTask[_]].getName ->
      new PersistanceMapper[StartTask[_], TaskStartedPersisted](TaskStartedPersisted.parseFrom),
    classOf[TimerTask.EndTime].getName ->
      new PersistanceMapper[TimerTask.EndTime, TimerTaskEndTimePersisted](TimerTaskEndTimePersisted.parseFrom),
    classOf[TimerTask.TimerExpired].getName ->
      new PersistanceMapper[TimerTask.TimerExpired, TimerTaskTimerExpiredPersisted](TimerTaskTimerExpiredPersisted.parseFrom),
    classOf[ProcessRepoPrivateApi.ProcessStarted[_]].getName ->
      new PersistanceMapper[ProcessRepoPrivateApi.ProcessStarted[_], ProcessRepoPrivateApiProcessStartedPersisted](ProcessRepoPrivateApiProcessStartedPersisted.parseFrom),
    classOf[ProcessContext].getName ->
      new PersistanceMapper[ProcessContext, ProcessContextPersisted](ProcessContextPersisted.parseFrom),
    classOf[ProcessRepo.ProcessFinished].getName ->
      new PersistanceMapper[ProcessRepo.ProcessFinished, ProcessRepoProcessFinishedPersisted](ProcessRepoProcessFinishedPersisted.parseFrom))
}
