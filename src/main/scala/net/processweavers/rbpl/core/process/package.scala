package net.processweavers.rbpl.core

import java.util.UUID

import akka.actor.Props
import play.api.libs.json._

import scala.util.control.NonFatal

package object process {

  case class CorrelationId(s: String)

  case class ProcessId(id: UUID)
  object ProcessId {
    def create = ProcessId(UUID.randomUUID())
  }

  case class ProcessName(n: String)

  case class ProcessContext(processId: ProcessId, correlationId: CorrelationId, name: ProcessName, description: String = "") extends Context

  private[core] trait UntypedProcessActivator {
    def propsForUntyped(processContext: Context, i: Initializer[_])(): Props
  }

  trait ProcessActivator[P] extends UntypedProcessActivator {
    def propsFor(processContext: ProcessContext, initializer: Initializer[P])(): Props
    def propsForUntyped(processContext: Context, initializer: Initializer[_])(): Props =
      propsFor(processContext.asInstanceOf[ProcessContext], initializer.asInstanceOf[Initializer[P]])
  }

  object Serializers {
    implicit val processNameFormat = new Format[ProcessName] {
      def writes(o: ProcessName): JsValue = JsString(o.n)
      def reads(json: JsValue): JsResult[ProcessName] = json match {
        case JsString(s) => {
          try {
            JsSuccess(ProcessName(s))
          } catch {
            case NonFatal(e) => JsError(s"can not parse '$s' as processname")
            case e: Throwable => throw e
          }
        }
        case _ => JsError(s"unexpected JSON type in '$json', expected 'JsString'")
      }
    }
    implicit val processIdFormat = new Format[ProcessId] {
      def writes(o: ProcessId): JsValue = JsString(o.id.toString)
      def reads(json: JsValue): JsResult[ProcessId] = json match {
        case JsString(s) => {
          try {
            JsSuccess(ProcessId(UUID.fromString(s)))
          } catch {
            case NonFatal(e) => JsError(s"can not parse '$s' as processname")
            case e: Throwable => throw e
          }
        }
        case _ => JsError(s"unexpected JSON type in '$json', expected 'JsString'")
      }
    }
  }

}
