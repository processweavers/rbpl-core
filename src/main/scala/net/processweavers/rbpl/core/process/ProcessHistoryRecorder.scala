package net.processweavers.rbpl.core.process

import java.time.Instant

import net.processweavers.rbpl.core.task._

class ProcessHistoryRecorder(val name: String, val processPayload: Option[Any], processHistory: ProcessHistoryRecorder.History = ProcessHistoryRecorder.History()) {

  import ProcessHistoryRecorder._

  def recordTaskStarted(td: TaskDescriptor): ProcessHistoryRecorder =
    new ProcessHistoryRecorder(
      name,
      processPayload,
      processHistory.copy(
        tasks = processHistory.tasks + (td.context.taskId -> Task(td.context.taskId, td.name, td.initializerDescriptor)),
        transitions = Transition(td.context.mayBeInitiatingResult, Some(td.context.taskId), td.context.mayBeTransistionName) :: processHistory.transitions,
        lastUpdated = Instant.now()))

  def recordTaskResult(result: TaskResult) =
    new ProcessHistoryRecorder(
      name,
      processPayload,
      processHistory.copy(
        results = processHistory.results + (result.taskContext.taskId -> Result(result.resultId, result.taskContext.taskId, result.description)),
        lastUpdated = Instant.now()))

  def recordTaskStopped(taskId: TaskId): ProcessHistoryRecorder = {
    val t = processHistory.tasks(taskId).copy(isStopped = true)
    new ProcessHistoryRecorder(
      name,
      processPayload,
      processHistory.copy(
        tasks = processHistory.tasks + (t.taskId -> t),
        lastUpdated = Instant.now()))
  }

  def history: History = processHistory.copy(label = name, processPayload = processPayload.map(_.toString).getOrElse(""))
}

object ProcessHistoryRecorder {
  case class History(label: String = "", processPayload: String = "", tasks: Map[TaskId, Task] = Map.empty, results: Map[TaskId, Result] = Map.empty, transitions: List[Transition] = Nil, lastUpdated: Instant = Instant.now())
  case class Task(taskId: TaskId, name: TaskName, initializerDescription: String, isStopped: Boolean = false)
  case class Result(resultId: ResultId, taskId: TaskId, description: String)
  case class Transition(from: Option[ResultId], to: Option[TaskId], name: Option[String])
}