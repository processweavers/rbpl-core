package net.processweavers.rbpl.core.env

import java.util.concurrent.TimeUnit

import akka.actor.{ ActorRef, ActorSystem }
import akka.util.Timeout

import scala.concurrent.Future
import scala.util.Failure

object PrincipalProvider {
  val ACTORNAME = "PrincipalProvider"

  case class GetPrincipal(id: String)

  case class PrincipalNotFound(id: String)
  case class Principal(id: String, pw: String)
}

object PrincipalProviderAccessor {
  // Short timeout only, since the actor must already exist
  implicit val timeout = Timeout(length = 300, TimeUnit.MILLISECONDS)
  def apply()(implicit system: ActorSystem): Future[ActorRef] = {
    import system.dispatcher
    val fa = system.actorSelection(s"/user/${PrincipalProvider.ACTORNAME}").resolveOne()
    fa.onComplete {
      case Failure(t) => system.log.error("PrincipalProvider can not be resolved! Not running?")
      case _ =>
    }
    fa
  }
}

