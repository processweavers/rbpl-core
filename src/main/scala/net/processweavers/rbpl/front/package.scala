package net.processweavers.rbpl

import akka.actor.ActorSystem
import akka.http.scaladsl.model.{ HttpEntity, Uri }
import akka.http.scaladsl.server.Directives.pathPrefix
import akka.http.scaladsl.server.directives.BasicDirectives
import akka.http.scaladsl.server.{ Directive0, Route }
import akka.stream.scaladsl.Framing
import akka.util.ByteString
import net.processweavers.rbpl.util.LocationAwareness

package object front {

  trait RouteBase {
    def fullyQualifiedDomainBasePath(implicit system: ActorSystem) = Uri(system.settings.config.getString("vfPortal.baseUrl"))
  }

  object BasePathBuilder extends RouteBase {
    def forSegment(segmentName: String)(implicit system: ActorSystem): Uri = fullyQualifiedDomainBasePath + segmentName + Uri./
  }

  trait ProcessRoute extends RouteBase with ProcessActorDirective {

    val processNameSegment: String
    def processRoute: Route

    def basePath(implicit system: ActorSystem): Uri = BasePathBuilder.forSegment(processNameSegment)

    def route: Route = {
      pathPrefix(processNameSegment) {
        processRoute
      }
    }
  }

}
