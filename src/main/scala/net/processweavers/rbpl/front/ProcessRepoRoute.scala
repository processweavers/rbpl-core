package net.processweavers.rbpl.front

import java.util.concurrent.TimeUnit

import akka.actor.{ ActorRef, ActorSystem }
import akka.pattern.ask
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.directives.{ AuthenticationDirective, Credentials }
import akka.util.Timeout
import net.processweavers.rbpl.core.env.PrincipalProvider.{ GetPrincipal, Principal }
import net.processweavers.rbpl.core.process.CorrelationId
import net.processweavers.rbpl.core.process.Process.{ ActiveTasks, AllActiveTasks }
import net.processweavers.rbpl.core.process.ProcessRepo.{ FindWithCorrelationId, FindWithPredicate, FinderResult, FinderResults }
import de.heikoseeberger.akkahttpplayjson.PlayJsonSupport
import net.processweavers.rbpl.core.env.PrincipalProviderAccessor
import net.processweavers.rbpl.core.process.ProcessRepo

import scala.concurrent.duration._
import scala.concurrent.Future
import scala.util.Success

trait ProcessRepoRoute extends PlayJsonSupport {

  implicit val timeout = Timeout(length = 500, TimeUnit.MILLISECONDS)

  def route(implicit system: ActorSystem): Route = {

    import net.processweavers.rbpl.core.process.Serializers._
    import net.processweavers.rbpl.core.task.Serializers._

    implicit val ec = system.dispatcher

    // format: OFF
    pathPrefix("processes") {
      authenticatePrincipal(system) { _ =>
        path(Segment / "activeTasks") { cId =>
          get {
            onSuccess(ProcessRepo()) { pr =>
              onSuccess((pr ? FindWithCorrelationId(CorrelationId(cId))).mapTo[FinderResult]) { fr =>
                fr.maybeProcess match {
                  case None => reject
                  case Some(p) =>
                    onSuccess((p._2 ? AllActiveTasks).mapTo[ActiveTasks]) { ats =>
                      complete(ats.ts.map{case (id,(n,_)) => (id, n)}.toList)
                    }
                }
              }
            }
          }
        } ~
        pathEnd {
          get {
            onSuccess(ProcessRepo()) { pr =>
              onSuccess((pr ? FindWithPredicate(ProcessRepo.FindAllPredicate)).mapTo[FinderResults]) { fr =>
                complete(fr.maybeProcesses.map(_._1.processId))
              }
            }
          }
        }
      }
    }
    // format: ON

  }

  def authenticatePrincipal(implicit system: ActorSystem, ps: Option[ActorRef] = None): AuthenticationDirective[Principal] = {

    def principalAuthenticator(credentials: Credentials)(implicit system: ActorSystem): Future[Option[Principal]] = {
      implicit val ec = system.dispatcher
      implicit val timeout = Timeout(5.seconds)
      credentials match {
        case provided @ Credentials.Provided(id) =>

          PrincipalProviderAccessor().flatMap(a => (a ? GetPrincipal(id)).map {
            case p: Principal if provided.verify(p.pw) => Some(p)
            case _ => None
          })

        case _ => Future.successful(None)
      }
    }

    authenticateBasicAsync(realm = "Participants", principalAuthenticator)

  }

}
