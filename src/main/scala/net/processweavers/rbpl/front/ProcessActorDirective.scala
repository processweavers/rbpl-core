package net.processweavers.rbpl.front

import java.util.concurrent.TimeUnit

import akka.actor.{ ActorRef, ActorSystem }
import akka.http.scaladsl.server.Directive1
import akka.http.scaladsl.server.Directives.{ extract, onComplete, onSuccess, reject }
import akka.pattern.ask
import akka.util.Timeout
import net.processweavers.rbpl.core.process._

import scala.concurrent.{ ExecutionContext, Future }
import scala.util.{ Failure, Success }

trait ProcessActorDirective {

  implicit val system: ActorSystem
  implicit val ec: ExecutionContext

  def processActorByCId(correlationId: CorrelationId): Directive1[ActorRef] = {

    implicit val timeout = Timeout(500, TimeUnit.MILLISECONDS)

    onSuccess(ProcessRepo()).flatMap { pr =>
      val f: Future[ActorRef] = (pr ? ProcessRepo.FindWithCorrelationId(correlationId)).map {
        case ProcessRepo.FinderResult(Some((_, processActor))) =>
          processActor
      }
      onComplete(f).flatMap {
        case Success(ar) =>
          extract(ctx => ar)
        case Failure(t) =>
          reject
      }
    }
  }
}
